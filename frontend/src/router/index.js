import { createRouter, createWebHistory } from "vue-router";
import AdminView from "@/views/AdminView";
import QueueView from "@/views/QueueView";
import ExercisesView from "@/views/ExercisesView";
import { Role } from "@/utils/role";
import store from "@/store";

const routes = [
  {
    path: "/",
    meta: { authorize: [] },
    redirect: () => {
      if (store.getters.isAdmin) return { name: "admin" };
      else return { name: "home" };
    },
  },
  {
    path: "/courses/:id/students",
    name: "courseStudents",
    meta: { authorize: [Role.Studas] },
    component: () =>
      import(
        /* webpackChunkName: "courseStudents" */ "../views/CourseStudentsView.vue"
      ),
  },
  {
    path: "/home",
    name: "home",
    meta: { authorize: [Role.User] },
    component: () =>
      import(/* webpackChunkName: "home" */ "../views/HomeView.vue"),
  },
  {
    path: "/login",
    name: "login",
    component: () =>
      import(/* webpackChunkName: "about" */ "../views/LoginView.vue"),
  },
  {
    path: "/settings",
    name: "settings",
    meta: { authorize: [] },
    component: () =>
      import(/* webpackChunkName: "about" */ "../views/SettingsView.vue"),
  },
  {
    path: "/admin",
    name: "admin",
    meta: { authorize: [Role.Admin] },
    component: AdminView,
  },
  {
    path: "/courses/:id/queue",
    name: "queue",
    meta: { authorize: [] },
    component: QueueView,
  },
  {
    path: "/courses/:id/exercises/:studId?",
    name: "exercises",
    meta: { authorize: [] },
    component: ExercisesView,
  },
  { path: "/:pathMatch(.*)*", redirect: "/" },
];

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes,
});

router.beforeEach(async (to, from, next) => {
  // redirect to login page if not logged in and trying to access a restricted page
  const { authorize } = to.meta;
  // const currentUser = authUtil.currentUserValue;

  await store.restored;

  let authorized = String(store.state.authorities).includes(String(authorize));

  if (authorize) {
    if (!store.state.user) {
      // not logged in so redirect to login page with the return url
      return next({ path: "/login", query: { returnUrl: to.path } });
    }

    // check if route is restricted by role
    if (authorize.length && !authorized) {
      console.log({ AUTHORIZED: authorized });
      // role not authorised so redirect to home page
      return next({ path: "/" });
    }
  }
  next();
});

export default router;
