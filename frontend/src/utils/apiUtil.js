import axios from "axios";
import { SERVER_CONTEXT } from "@/utils/config";
import loginForm from "@/components/LoginForm";

const api_server = SERVER_CONTEXT;

export const api = {
  postObject,
  post,
  delete_,
  get,
};

function get(url) {
  return axios
    .get(api_server + url)
    .then((response) => {
      console.log(response.data);
      return response.data;
    })
    .catch((e) => {
      console.log(e);
    });
}

function postObject(url, object) {
  return axios
    .post(api_server + url, object)
    .then((response) => {
      console.log(response.data);
      return response.data;
    })
    .catch((e) => {
      console.log(e);
    });
}

function delete_(url) {
  return axios
    .delete(api_server + url)
    .then((response) => {
      console.log(response.data);
      return response.data;
    })
    .catch((e) => {
      console.log(e);
    });
}

function post(url) {
  return axios
    .post(api_server + url)
    .then((response) => {
      console.log(response.data);
      return response.data;
    })
    .catch((e) => {
      console.log(e);
    });
}
