import axios from "axios";
import { BehaviorSubject } from "rxjs";
import { SERVER_CONTEXT } from "@/utils/config";

const currentUserSubject = new BehaviorSubject(
  JSON.parse(localStorage.getItem("currentUser"))
);

export const authUtil = {
  login,

  getToken,
};

async function login(loginRequest, token) {
  const config = {
    headers: {
      Authorization: `Bearer ${token}`,
    },
  };
  return axios
    .post(SERVER_CONTEXT + "/login", loginRequest, config)
    .then((response) => {
      console.log(response.data);

      return response;
    })
    .catch((err) => {
      console.log(err);
    });
}

function getToken(loginRequest) {
  const config = {
    headers: {
      "content-type": "application/json",
    },
  };
  return axios
    .post(SERVER_CONTEXT + "/token", loginRequest, config)
    .then((response) => {
      return response;
    })
    .catch((err) => {
      console.log(err);
    });
}
