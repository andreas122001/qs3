import { createStore } from "vuex";
import { authUtil } from "@/utils/authUtil";
import VuexPersistence from "vuex-persist";
import localForage from "localforage";
import { Role } from "@/utils/role";

const vuexLocal = new VuexPersistence({
  storage: localForage,
  asyncStorage: true,
});

export default createStore({
  state: {
    user: {},
    token: "",
    authorities: "",
  },
  getters: {
    isLoggedIn(state) {
      return state.user !== null;
    },
    isAdmin(state) {
      return state.authorities.includes(Role.Admin);
    },
  },
  mutations: {
    SET_ID(state, id) {
      state.id = id;
    },
    SET_USER(state, user) {
      state.user = user;
    },
    SET_TOKEN(state, token) {
      state.token = token;
    },
    SET_AUTHORITIES(state, authorities) {
      state.authorities = authorities;
    },
  },
  actions: {
    logout({ commit }) {
      commit("SET_USER", null);
    },

    async login({ commit }, login) {
      let response = await authUtil.getToken(login);

      if (response.status !== 200) return false;

      console.log(response.data);
      commit("SET_TOKEN", response.data.token);
      commit("SET_AUTHORITIES", response.data.authorities);

      let user = await authUtil
        .login(login, response.data.token)
        .catch((error) => {
          console.log(error);
          return false;
        });

      if (user.status !== 200) return false;

      console.log(user.data);

      commit("SET_USER", user.data);

      return true;
    },

    setToken({ commit }, token) {
      commit("SET_TOKEN", token);
    },
    setAuthorities({ commit }, authorities) {
      commit("SET_AUTHORITIES", authorities);
    },
  },
  modules: {},
  plugins: [vuexLocal.plugin],
});
