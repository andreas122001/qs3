
-- User tables:
CREATE TABLE IF NOT EXISTS user (
    id INT AUTO_INCREMENT PRIMARY KEY,
    email VARCHAR(255) UNIQUE,
    first_name VARCHAR(20) NOT NULL,
    last_name VARCHAR(20) NOT NULL,
    password VARCHAR(60) NOT NULL,
    created DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP
);

CREATE TABLE IF NOT EXISTS student (
    id INT PRIMARY KEY,
    FOREIGN KEY (id) REFERENCES user(id)
        ON DELETE CASCADE
);

CREATE TABLE IF NOT EXISTS studas (
    id INT PRIMARY KEY,
    FOREIGN KEY (id) REFERENCES user(id)
        ON DELETE CASCADE
);

CREATE TABLE IF NOT EXISTS admin (
    id INT PRIMARY KEY,
    FOREIGN KEY (id) REFERENCES user(id)
        ON DELETE CASCADE
);



-- Course table
CREATE TABLE IF NOT EXISTS course (
    id INT AUTO_INCREMENT PRIMARY KEY,
    code VARCHAR(20) NOT NULL,
    year VARCHAR(4) NOT NULL,
    name VARCHAR(20) NOT NULL,
    description VARCHAR(50),
    UNIQUE(code,year)
);

CREATE TABLE IF NOT EXISTS student_course (
    stud_id int NOT NULL,
    course_id int NOT NULL,
    PRIMARY KEY (stud_id, course_id),
    FOREIGN KEY (stud_id) REFERENCES student(id)
        ON DELETE CASCADE,
    FOREIGN KEY (course_id) REFERENCES course(id)
        ON DELETE CASCADE
);

CREATE TABLE IF NOT EXISTS studas_course (
    studas_id int NOT NULL,
    course_id int NOT NULL,
    PRIMARY KEY (studas_id, course_id),
    FOREIGN KEY (studas_id) REFERENCES studas(id)
        ON DELETE CASCADE,
    FOREIGN KEY (course_id) REFERENCES course(id)
        ON DELETE CASCADE
);



-- Exercises
CREATE TABLE IF NOT EXISTS exercise (
    id int AUTO_INCREMENT PRIMARY KEY,
    course_id int NOT NULL,
    name VARCHAR(20) NOT NULL,
    FOREIGN KEY (course_id) REFERENCES course(id)
        ON DELETE CASCADE
);

CREATE TABLE IF NOT EXISTS student_exercise (
    stud_id int NOT NULL,
    exercise_id int NOT NULL,
    PRIMARY KEY (stud_id, exercise_id),
    UNIQUE (stud_id, exercise_id),
    FOREIGN KEY (stud_id) REFERENCES student(id)
        ON DELETE CASCADE,
    FOREIGN KEY (exercise_id) REFERENCES exercise(id)
        ON DELETE CASCADE
);



-- Queue
CREATE TABLE IF NOT EXISTS queue (
    id int AUTO_INCREMENT PRIMARY KEY,
    course_id int NOT NULL,
    FOREIGN KEY (course_id) REFERENCES course(id)
        ON DELETE CASCADE
);

CREATE TABLE IF NOT EXISTS student_queue (
    stud_id int,
    queue_id int,
    PRIMARY KEY(stud_id, queue_id),
    FOREIGN KEY (stud_id) REFERENCES student(id)
        ON DELETE CASCADE,
    FOREIGN KEY (queue_id) REFERENCES queue(id)
        ON DELETE CASCADE
);
