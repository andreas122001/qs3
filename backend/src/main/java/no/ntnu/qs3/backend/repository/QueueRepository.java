package no.ntnu.qs3.backend.repository;

import no.ntnu.qs3.backend.model.User;
import no.ntnu.qs3.backend.model.UserRequest;
import no.ntnu.qs3.backend.model.UserResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class QueueRepository {

    @Autowired
    JdbcTemplate jdbcTemplate;

    public boolean save(int courseId) {
        String sql = "INSERT INTO queue (course_id) VALUES (?);";

        return jdbcTemplate.update(sql, courseId) > 0;
    }

    public boolean existsByCourseId(int courseId) {
        String sql = "SELECT * FROM queue WHERE courseId = ?";

        return jdbcTemplate.queryForObject(sql, Integer.class, courseId) > 0;
    }

    public List<User> getStudentsByCourseId(int courseId) {
        String sql = "SELECT u.* FROM user u JOIN student s ON u.id = s.id " +
                "WHERE s.id IN (SELECT stud_id FROM student_queue s JOIN queue q ON s.id = q.id WHERE q.course_id = ?);";

        return jdbcTemplate.query(sql,
                BeanPropertyRowMapper.newInstance(User.class), courseId);
    }

    public boolean saveUserToQueue(int studId, int courseId) {
        String sql = "INSERT INTO student_queue (stud_id, queue_id) VALUES " +
                "(?, (SELECT queue_id FROM queue WHERE course_id = ?) );";

        return jdbcTemplate.update(sql,  studId, courseId) > 0;
    }



}
