package no.ntnu.qs3.backend.controller;

import no.ntnu.qs3.backend.model.LoginRequest;
import no.ntnu.qs3.backend.model.User;
import no.ntnu.qs3.backend.model.UserResponse;
import no.ntnu.qs3.backend.repository.UserRepository;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import static no.ntnu.qs3.backend.util.ModelConverter.userToResponse;

/**
 * Controller for accepting login-requests.
 */
@RestController
@RequestMapping(value = "")
@CrossOrigin
public class LoginController {

    private static final Logger LOGGER = LogManager.getLogger(LoginController.class);

    @Autowired
    private UserRepository userRepo;

    /**
     * Logs a user in given a loginrequest with login credentials.
     * @param request credentials for login.
     * @return user-info.
     */
    @PostMapping(value = "/login", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<UserResponse> doLogin(final @RequestBody LoginRequest request) {
        LOGGER.info("Requested user login: " + request.getUsername());

        User user = userRepo.findByEmail(request.getUsername());

        if (user!=null)
            if(user.getPassword().equals(request.getPassword()))
                return new ResponseEntity<>(userToResponse(user), HttpStatus.OK);
        return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
    }
}