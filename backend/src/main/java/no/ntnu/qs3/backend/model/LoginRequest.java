package no.ntnu.qs3.backend.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Request with credentials.
 */
public class LoginRequest {
    private final String username;
    private final String password;

    @JsonCreator
    public LoginRequest(@JsonProperty("username") final String username,
                        @JsonProperty("password") final String password) {
        this.username = username;
        this.password = password;
    }

    @JsonProperty("username")
    public String getUsername() {
        return username;
    }

    @JsonProperty("password")
    public String getPassword() {
        return password;
    }

}
