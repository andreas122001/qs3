package no.ntnu.qs3.backend.service;

import no.ntnu.qs3.backend.model.UserRequest;
import no.ntnu.qs3.backend.model.UserResponse;
import no.ntnu.qs3.backend.repository.StudasRepository;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Service for handling student assistants.
 */
@Service
public class StudasService {

    private static final Logger logger = LogManager.getLogger(StudasService.class);

    @Autowired
    StudasRepository repo;

    @Autowired
    UserService userService;


    /**
     * Gets a student assistant by id.
     * @param id of student assistant.
     * @return the student assistant.
     */
    public UserResponse getById(int id) {
        logger.debug("Finding studas by id " + id + "...");
        UserResponse studas = null;

        // If studas exists, find associated user in user repo
        if (repo.existsById(id)) {
            studas = userService.getById(id);
            logger.debug("Found studas: " + studas);
        } else logger.debug("Studas not found.");

        return studas;
    }


    /**
     * Removes a student assistant from the database.
     * @param id of student assistant.
     * @return removed student assistant.
     */
    public UserResponse removeById(int id) {
        UserResponse user = userService.getById(id);

        if (user != null) {
            if (repo.deleteById(id))
                return user;
        }
        return null;
    }

    /**
     * finds a student assistant by email.
     * @param email to check with.
     * @return the student assistant.
     */
    public UserResponse getByEmail(String email) {
        UserResponse user = userService.getByEmail(email);

        if (user != null)
            if (repo.existsById(user.getId()))
                return user;

        return null;
    }

    /**
     * Checks if a students assistant exists, by id.
     * @param id of user.
     * @return true/false.
     */
    public boolean existsById(int id) {
        return repo.existsById(id);
    }

    public boolean existsByEmail(String email) {
        UserResponse user = userService.getByEmail(email);

        if (user != null) {
            return repo.existsById(user.getId());
        }
        return false;
    }

    /**
     * Creates a new student assistant.
     * @param studas user info.
     * @return created student assistant.
     */
    public UserResponse createNewStudas(UserRequest studas) {

        logger.debug("Creating studas: {}", studas);

        // If exists, return null
        if (existsByEmail(studas.getEmail())) {
            logger.debug("Student already exists.");
            return null;
        }

        // If user exists
        UserResponse user = userService.getByEmail(studas.getEmail());
        if (user != null) {
            logger.debug("User exists, adding to studas.");
            repo.save(user.getId());
            return user;
        }

        // If user does not exist, create user and add studas
        logger.debug("User does not exist, creating user...");
        UserResponse newUser = userService.createNewUser(studas);
        logger.debug("Adding new user '{}' to studas.", newUser);
        repo.save(newUser.getId());

        return newUser;
    }

}
