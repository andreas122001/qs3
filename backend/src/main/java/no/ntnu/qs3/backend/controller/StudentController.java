package no.ntnu.qs3.backend.controller;

import no.ntnu.qs3.backend.model.UserRequest;
import no.ntnu.qs3.backend.model.UserResponse;
import no.ntnu.qs3.backend.service.StudentService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

/**
 * Endpoint for creating, updating, getting and deleting students from the database.
 */
@RestController
@RequestMapping(value = "/students")
@CrossOrigin
public class StudentController {

    private static final Logger logger = LogManager.getLogger(StudentController.class);

    @Autowired
    StudentService service;

    /**
     * Adds a student to the database given user information. If user does not exist, it creates a new user.
     * @param request user info.
     * @return created user.
     */
    @PostMapping("")
    public ResponseEntity<UserResponse> addStudent(@RequestBody UserRequest request) {
        logger.info("Requested adding student: {}", request);
        UserResponse newStudent = service.createNewStudent(request);
        if (newStudent != null)
            return new ResponseEntity<>(newStudent, HttpStatus.CREATED);
        logger.debug("Student not added.");
        return new ResponseEntity<>(HttpStatus.CONFLICT);
    }

    /**
     * Gets a student given user-id.
     * @param id of student.
     * @return the student.
     */
    @GetMapping("/{id}")
    public ResponseEntity<UserResponse> getById(@PathVariable("id") int id) {
        logger.info("Requested student by id: " + id);
        UserResponse student = service.getById(id);
        if (student != null)
            return new ResponseEntity<>(student, HttpStatus.OK);
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }


    /** TODO: ADMIN
     * Removes a student by id.
     * @return removed student.
     */
    @DeleteMapping("/{id}")
    @ResponseStatus
    public ResponseEntity<UserResponse> removeStudent(@PathVariable("id") int id) {
        logger.info("Requested remove student with id: {}", id);
        UserResponse user = service.removeById(id);
        if (user != null)
            return new ResponseEntity<>(user, HttpStatus.OK);
        logger.warn("Student not removed.");
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

}
