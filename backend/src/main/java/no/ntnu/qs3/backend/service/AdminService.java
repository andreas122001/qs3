package no.ntnu.qs3.backend.service;

import no.ntnu.qs3.backend.repository.AdminRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Handles AdminRepository.
 */
@Service
public class AdminService {

    @Autowired
    private AdminRepository repo;

    /**
     * Checks if an admin exists with given id.
     * @param id to check.
     * @return if id is an admin.
     */
    public boolean existsById(int id) {
        return repo.existsById(id);
    }
}
