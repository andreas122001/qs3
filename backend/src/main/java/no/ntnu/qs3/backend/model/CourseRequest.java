package no.ntnu.qs3.backend.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Objects;

/**
 * A request as JSON
 */
public class CourseRequest {

    private String name;
    private String code;
    private String description;
    private int year;

    @JsonCreator
    public CourseRequest(final @JsonProperty("name") String name,
                         final @JsonProperty("code") String code,
                         final @JsonProperty("description") String description,
                         final @JsonProperty("year") int year) {
        this.name = name;
        this.code = code;
        this.description = description;
        this.year = year;
    }


    @JsonProperty("year")
    public int getYear() {
        return year;
    }

    @JsonProperty("year")
    public void setYear(int year) {
        this.year = year;
    }

    @JsonProperty("name")
    public String getName() {
        return name;
    }

    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    @JsonProperty("code")
    public String getCode() {
        return code;
    }

    @JsonProperty("code")
    public void setCode(String code) {
        this.code = code;
    }

    @JsonProperty("description")
    public String getDescription() {
        return description;
    }

    @JsonProperty("description")
    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return "'" + code + " [" + year + "]'";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CourseRequest course = (CourseRequest) o;
        return year == course.year && Objects.equals(code, course.code);
    }

    @Override
    public int hashCode() {
        return Objects.hash(year, code);
    }


}
