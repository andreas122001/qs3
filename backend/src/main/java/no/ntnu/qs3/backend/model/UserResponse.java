package no.ntnu.qs3.backend.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Objects;

/**
 * User data with id as JSON.
 */
public class UserResponse {

    private int id;
    private String email;
    private String lastname;
    private String firstname;

    public UserResponse(@JsonProperty("id") int id,
                        @JsonProperty("email") String email,
                        @JsonProperty("lastname") String lastname,
                        @JsonProperty("firstname") String firstname) {
        this.id = id;
        this.email = email;
        this.lastname = lastname;
        this.firstname = firstname;
    }

    @Override
    public String toString() {
        return "("+id+")["+email+"] '"+ lastname + ", " + firstname+"'";
    }

    @JsonProperty("id")
    public int getId() {
        return id;
    }

    @JsonProperty("id")
    public void setId(int id) {
        this.id = id;
    }

    @JsonProperty("email")
    public String getEmail() {
        return email;
    }

    @JsonProperty("email")
    public void setEmail(String email) {
        this.email = email;
    }

    @JsonProperty("lastname")
    public String getLastname() {
        return lastname;
    }

    @JsonProperty("lastname")
    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    @JsonProperty("firstname")
    public String getFirstname() {
        return firstname;
    }

    @JsonProperty("firstname")
    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UserResponse that = (UserResponse) o;
        return Objects.equals(email, that.email);
    }

    @Override
    public int hashCode() {
        return Objects.hash(email);
    }




}
