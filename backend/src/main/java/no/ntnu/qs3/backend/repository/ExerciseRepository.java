package no.ntnu.qs3.backend.repository;

import no.ntnu.qs3.backend.model.Exercise;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Repository that handles the Exercise-table in the database.
 */
@Repository
public interface ExerciseRepository extends CrudRepository<Exercise, Integer> {

    Exercise getByCourseIdAndId(int courseId, int id);

    Exercise deleteByCourseIdAndId(int courseId, int id);

    List<Exercise> getAllByCourseId(int courseId);

    Exercise getById(int id);

}
