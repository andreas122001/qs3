package no.ntnu.qs3.backend.service;

import no.ntnu.qs3.backend.model.Exercise;
import no.ntnu.qs3.backend.model.ExerciseRequest;
import no.ntnu.qs3.backend.model.ExerciseResponse;
import no.ntnu.qs3.backend.repository.ExerciseRepository;
import no.ntnu.qs3.backend.repository.StudentExerciseRepository;
import no.ntnu.qs3.backend.util.ModelConverter;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

import static no.ntnu.qs3.backend.util.ModelConverter.exerciseToResponse;

/**
 * Service of ExerciseController. Creates/deletes/returns exercises at a course.
 * Also handles students completed exercises.
 */
@Service
public class ExerciseService {

    private static final Logger logger = LogManager.getLogger(ExerciseService.class);

    @Autowired
    ExerciseRepository repo;

    @Autowired
    StudentExerciseRepository studExRepo;

    /**
     * Gets all exercises given course id.
     * @param id of course.
     * @return exercises as a list.
     */
    public List<ExerciseResponse> getExercisesByCourseId(int id) {
        List<Exercise> response = repo.getAllByCourseId(id);
        logger.debug(response.isEmpty() ?
                "No exercises found." : "Found "+response.size()+ " exercises.");
        return response.stream().map(ModelConverter::exerciseToResponse).collect(Collectors.toList());
    }

    /**
     * Creates an exercise tied to a course.
     * @param courseId id of course.
     * @param request exercise to create.
     * @return created exercise.
     */
    public ExerciseResponse createNewExerciseAtCourse(int courseId,
                                                      ExerciseRequest request) {
        Exercise exerciseRef = new Exercise(courseId, request.getName());
        Exercise response = repo.save(exerciseRef);
        logger.debug("Exercise '{}' created!", response);
        return exerciseToResponse(response);
    }

    /**
     * Gets a exercise given a course id and exercise id.
     * @param courseId id of course.
     * @param exerciseId id of exercise.
     * @return exercise.
     */
    public ExerciseResponse getExerciseAtCourseById(int courseId, int exerciseId) {
        Exercise response = repo.getByCourseIdAndId(courseId, exerciseId);
        logger.debug(response == null ? "Exercise not found." : "Found exercise: " + response);
        return response == null ? null : exerciseToResponse(response);
    }

    /**
     * Deletes an exercise given course id and exercise id.
     * @param courseId id of course.
     * @param exerciseId if of exercise.
     * @return deleted exercise.
     */
    public ExerciseResponse deleteExerciseAtCourseById(int courseId, int exerciseId) {
        Exercise response = repo.deleteByCourseIdAndId(courseId, exerciseId);
        logger.debug(response == null ?
                "Exercise was not deleted." : "Exercise " + response + " deleted!");
        return response == null ? null : exerciseToResponse(response);
    }

    /**
     * Gets an exercise by id.
     * NOTE: id was intended to be unique only per course, but it wasn't practical
     * because Spring wouldn't allow it easily...
     * That's why exercise-id is now unique.
     * @param id of exercise.
     * @return exercise.
     */
    public ExerciseResponse getExerciseById(int id) {
        Exercise response = repo.getById(id);
        logger.debug(response == null ?
                "Exercise not found." : "Found exercise '{}'.", response);
        return response==null ? null : exerciseToResponse(response);
    }

    /**
     * Gets a list of all exercises a student has completed at a course.
     * @param studId id of student.
     * @param courseId id of course.
     * @return list of exercises.
     */
    public List<ExerciseResponse> getStudentCompletedExercises(int studId, int courseId) {
        List<Exercise> response = studExRepo.findAllByStudIdAndCourseId(studId, courseId);
        logger.debug(response.isEmpty() ?
                "No exercises found." : "Found " + response.size() + " exercises.");
        return response.stream().map(ModelConverter::exerciseToResponse).collect(Collectors.toList());
    }

    /**
     * Adds exercise to students completed exercises.
     * @return added exercise.
     */
    public ExerciseResponse addCompletedExerciseToStudent(int studId, int exerciseId) {
        boolean saved = studExRepo.save(studId, exerciseId);
        logger.debug(!saved?"Exercise not approved." : "Exercise approved!");
        return !saved ? null : getExerciseById(exerciseId);
    }

    /**
     * Deletes a exercise from a students completed exercises.
     * @param studId id of student.
     * @return deleted exercise.
     */
    public ExerciseResponse deleteCompletedExerciseFromStudent(int studId, int exerciseId) {
        boolean deleted = studExRepo.deleteByStudIdAndExerciseId(studId, exerciseId);
        logger.debug(!deleted?"Exercise not unapproved." : "Exercise successfully unapproved!");
        return !deleted ? null : getExerciseById(exerciseId);
    }

}
