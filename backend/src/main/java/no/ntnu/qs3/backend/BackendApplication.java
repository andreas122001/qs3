package no.ntnu.qs3.backend;

import no.ntnu.qs3.backend.model.CourseRequest;
import no.ntnu.qs3.backend.model.ExerciseRequest;
import no.ntnu.qs3.backend.model.User;
import no.ntnu.qs3.backend.model.UserRequest;
import no.ntnu.qs3.backend.repository.AdminRepository;
import no.ntnu.qs3.backend.repository.StudasRepository;
import no.ntnu.qs3.backend.repository.StudentRepository;
import no.ntnu.qs3.backend.repository.UserRepository;
import no.ntnu.qs3.backend.service.*;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Profile;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@SpringBootApplication
@EnableSwagger2
public class BackendApplication {

    public static void main(String[] args) {
        SpringApplication.run(BackendApplication.class, args);
    }


    /**
     * Fills database with test-data.
     */
    @Profile("!test")
    @Bean
    CommandLineRunner run(StudentService student, CourseService course, UserRepository user,
                          StudentCourseService studentCourse, StudasService studas,
                          StudasCourseService studasCourse, ExerciseService exercise, AdminRepository admin) {
        return args -> {

            user.save(new User("user", "user", "user","user"));
            user.save(new User("admin", "admin", "admin","admin"));
            student.createNewStudent(new UserRequest("user", "user","user"));
            studas.createNewStudas(new UserRequest("user", "user","user"));
            admin.save(2);

            course.createNewCourse(new CourseRequest("Fullstack", "IDATT2105", "", 2022));
            course.createNewCourse(new CourseRequest("Nettverk", "IDATT2106", "", 2022));
            course.createNewCourse(new CourseRequest("Databaser", "IDATT2104", "", 2022));

            studentCourse.addStudent(1,new UserRequest("user", "user","user"));
            studentCourse.addStudent(2,new UserRequest("user", "user","user"));

            studasCourse.addStudas(1, new UserRequest("user","user","user"));
            studasCourse.addStudas(2, new UserRequest("user","user","user"));
            studasCourse.addStudas(3, new UserRequest("user","user","user"));

            exercise.createNewExerciseAtCourse(1, new ExerciseRequest("Lab 1"));
            exercise.createNewExerciseAtCourse(1, new ExerciseRequest("Lab 2"));
            exercise.createNewExerciseAtCourse(1, new ExerciseRequest("Lab 3"));
            exercise.createNewExerciseAtCourse(1, new ExerciseRequest("Lab 4"));
            exercise.createNewExerciseAtCourse(1, new ExerciseRequest("Lab 5"));
            exercise.createNewExerciseAtCourse(1, new ExerciseRequest("Lab 6"));

            exercise.createNewExerciseAtCourse(2, new ExerciseRequest("Øving 1"));
            exercise.createNewExerciseAtCourse(2, new ExerciseRequest("Øving 2"));
            exercise.createNewExerciseAtCourse(2, new ExerciseRequest("Øving 3"));

            exercise.createNewExerciseAtCourse(3, new ExerciseRequest("Øving 1"));
            exercise.createNewExerciseAtCourse(3, new ExerciseRequest("Øving 2"));
            exercise.createNewExerciseAtCourse(3, new ExerciseRequest("Lab 1"));

            studentCourse.addStudent(1,new UserRequest("andrebw@stud.ntnu.no","Winje","Andreas"));
            studentCourse.addStudent(2,new UserRequest("andrebw@stud.ntnu.no","Winje","Andreas"));

            studentCourse.addStudent(1,new UserRequest("dabest@stud.ntnu.no","Student","Dabest"));
            studentCourse.addStudent(2,new UserRequest("dabest@stud.ntnu.no","Student","Dabest"));
            studentCourse.addStudent(3,new UserRequest("dabest@stud.ntnu.no","Student","Dabest"));

            exercise.addCompletedExerciseToStudent(3,1);
            exercise.addCompletedExerciseToStudent(4,2);
            exercise.addCompletedExerciseToStudent(4,4);
        };
    }
}
