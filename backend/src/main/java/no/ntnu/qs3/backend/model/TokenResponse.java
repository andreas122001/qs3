package no.ntnu.qs3.backend.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Response with a token and user-roles
 */
public class TokenResponse {

    private String token;
    private String authorities;

    @JsonCreator
    public TokenResponse(@JsonProperty("token") String token,
                         @JsonProperty("authorities") String authorities) {
        this.token = token;
        this.authorities = authorities;
    }

    @JsonProperty("token")
    public String getToken() {
        return token;
    }

    @JsonProperty("token")
    public void setToken(String token) {
        this.token = token;
    }

    @JsonProperty("authorities")
    public String getAuthorities() {
        return authorities;
    }

    @JsonProperty("authorities")
    public void setAuthorities(String authorities) {
        this.authorities = authorities;
    }
}
