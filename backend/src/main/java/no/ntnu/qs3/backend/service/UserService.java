package no.ntnu.qs3.backend.service;

import no.ntnu.qs3.backend.model.User;
import no.ntnu.qs3.backend.model.UserRequest;
import no.ntnu.qs3.backend.model.UserResponse;
import no.ntnu.qs3.backend.repository.UserRepository;
import no.ntnu.qs3.backend.util.ModelConverter;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

import static no.ntnu.qs3.backend.util.ModelConverter.userToResponse;
import static no.ntnu.qs3.backend.util.PasswordUtil.generatePassword;

/**
 * Service for dealing with users.
 */
@Service
public class UserService {

    private static final Logger logger = LogManager.getLogger(UserService.class);

    @Autowired
    UserRepository repo;

    /**
     * Creates a new user given user-info.
     * @param user user info.
     * @return created user.
     */
    public UserResponse createNewUser(UserRequest user) {
        logger.debug("Creating user: {}", user);
        if (repo.existsByEmail(user.getEmail())) {
            logger.debug("User already exists.");
            return null;
        }

        String password = generatePassword();
        password = new BCryptPasswordEncoder().encode(password);

        User userRef = new User(user.getEmail(),user.getFirstname(), user.getLastname(), password);
        User newUserRef = repo.save(userRef);

        logger.debug("Created user: {}", newUserRef);
        logger.debug("USER PASSWORD: " + password);

        // TODO: Send mail to user.

        return userToResponse(newUserRef);
    }

    /**
     * Gets all users in the database.
     * NOTE: paging required for future.
     * @return all users.
     */
    public List<UserResponse> getAll() {
        List<User> users = repo.findAll();
        logger.debug(users.isEmpty() ? "No courses found." : users.size() + " courses found.");
        return users.stream().map(ModelConverter::userToResponse).collect(Collectors.toList());
    }

    /**
     * Gets a user by id.
     * @param id of user.
     * @return the user.
     */
    public UserResponse getById(int id) {
        User user = repo.findById(id);
        logger.debug(user == null ? "User not found." : "Found user: " + user);
        return user == null ? null : userToResponse(user);
    }

    /**
     * Checks if a user exits by email.
     * @param email to check with.
     * @return true/false.
     */
    public boolean existsByEmail(String email) {
        return repo.existsByEmail(email);
    }

    /**
     * Finds a user by email.
     * @param email to check with.
     * @return the user.
     */
    public UserResponse getByEmail(String email) {
        User user = repo.findByEmail(email);
        if (user != null) {
            logger.debug("User found: " + user);
            return userToResponse(user);
        }
        logger.debug("User not found.");
        return null;
    }

}
