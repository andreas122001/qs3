package no.ntnu.qs3.backend.controller;

import no.ntnu.qs3.backend.model.CourseRequest;
import no.ntnu.qs3.backend.model.CourseResponse;
import no.ntnu.qs3.backend.service.CourseService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Endpoints for getting, creating and deleting courses.
 */
@RestController
@RequestMapping("/courses")
@CrossOrigin()
public class CourseController {

    private static final Logger logger = LogManager.getLogger(CourseController.class);

    @Autowired
    CourseService service;

    /** TODO: ADMIN
     * Adds a course to the database.
     * @param course to add.
     * @return created object.
     */
    @PostMapping(value = "", consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<CourseResponse> add(@RequestBody CourseRequest course) {
        logger.info("Requested creating new course: {}", course);

        CourseResponse response = service.createNewCourse(course);

        if (response != null)
            return new ResponseEntity<>(response, HttpStatus.CREATED);

        return new ResponseEntity<>(HttpStatus.CONFLICT);
    }


    /**
     * Gets a course by id.
     * @param id of course.
     * @return found course.
     */
    @GetMapping("/{id}")
    public ResponseEntity<CourseResponse> getById(@PathVariable("id") int id) {

        logger.info("Requested course {}", id);
        CourseResponse course = service.getById(id);

        if (course != null)
            return new ResponseEntity<>(course, HttpStatus.OK);

        return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
    }


    /**
     * Deletes a course by id.
     * @param id of course.
     * @return OK if successful, BAD REQUEST if failed.
     */
    @DeleteMapping("/{id}")
    public ResponseEntity<CourseResponse> deleteById(@PathVariable("id") int id) {
        logger.info("Requested deleting course {}", id);
        CourseResponse course = service.deleteById(id);
        if (course != null)
            return new ResponseEntity<>(course, HttpStatus.OK);
        else return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    /**
     * Gets all courses. Note: might require paging in the future.
     * @return all subjects.
     */
    @GetMapping("")
    public ResponseEntity<List<CourseResponse>> getAll() {

        logger.info("Requested all courses");
        List<CourseResponse> courses = service.getAll();

        if (courses != null && !courses.isEmpty())
            return new ResponseEntity<>(courses, HttpStatus.OK);

        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

}
