package no.ntnu.qs3.backend.controller;

import no.ntnu.qs3.backend.model.User;
import no.ntnu.qs3.backend.model.UserRequest;
import no.ntnu.qs3.backend.model.UserResponse;
import no.ntnu.qs3.backend.service.UserService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.jta.UserTransactionAdapter;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Controller for handling requests for getting/updating users.
 */
@RestController
@RequestMapping("/users")
@CrossOrigin
public class UserController {

    private static final Logger logger = LogManager.getLogger(UserController.class);

    @Autowired
    UserService service;

    /**
     * Gets all users in the database.
     * NOTE: should be replaced with paged-lookup in the future.
     * @return list of users.
     */
    @GetMapping(value = "", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<UserResponse>> getAllUsers() {
        logger.info("Requested all users.");

        List<UserResponse> users = service.getAll();
        if (users != null)
            return new ResponseEntity<>(users, HttpStatus.OK);
        else return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    /**
     * Creates a new user.
     * @param user user info.
     * @return created user.
     */
    @PostMapping(value = "", consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<UserResponse> createUser(@RequestBody UserRequest user) {
        logger.info("Requested add user: " + user);

        UserResponse newUser = service.createNewUser(user);
        if (newUser != null)
            return new ResponseEntity<>(newUser, HttpStatus.CREATED);
        else return new ResponseEntity<>(HttpStatus.CONFLICT);
    }


    /**
     * Gets a user by id.
     * @param id of user.
     * @return the user.
     */
    @GetMapping("/{id}")
    public ResponseEntity<UserResponse> getUser(@PathVariable("id") int id) {
        logger.info("Requested get user by id: {}", id);

        UserResponse user = service.getById(id);
        if (user != null)
            return new ResponseEntity<>(user, HttpStatus.OK);
        else return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }




}
