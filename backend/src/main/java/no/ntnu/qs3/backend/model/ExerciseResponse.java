package no.ntnu.qs3.backend.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Objects;

/**
 * Exercise as JSON.
 */
public class ExerciseResponse {

    private int id;
    private int courseId;
    private String name;

    public ExerciseResponse(@JsonProperty("id") int id,
                            @JsonProperty("courseId") int courseId,
                            @JsonProperty("name") String name) {
        this.id = id;
        this.courseId = courseId;
        this.name = name;
    }

    public String toString() {
        return name;
    }

    @JsonProperty("id")
    public int getId() {
        return id;
    }

    @JsonProperty("id")
    public void setId(int id) {
        this.id = id;
    }

    @JsonProperty("courseId")
    public int getCourseId() {
        return courseId;
    }

    @JsonProperty("courseId")
    public void setCourseId(int courseId) {
        this.courseId = courseId;
    }

    @JsonProperty("name")
    public String getName() {
        return name;
    }

    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ExerciseResponse exercise = (ExerciseResponse) o;
        return id == exercise.id && courseId == exercise.courseId;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, courseId);
    }
}

