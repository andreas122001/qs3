package no.ntnu.qs3.backend.repository;

import no.ntnu.qs3.backend.model.Course;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CourseRepository extends CrudRepository<Course, Integer> {

    Course findById(int id);

    List<Course> findAllById(int id);

    List<Course> findAll();

    boolean existsByCodeAndYear(String code, int year);

}
