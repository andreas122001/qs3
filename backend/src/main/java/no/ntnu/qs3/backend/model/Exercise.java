package no.ntnu.qs3.backend.model;

import org.springframework.data.annotation.Id;

import java.util.Objects;

/**
 * Model of an exercise.
 */
public class Exercise {

    @Id
    private int id;
    private int courseId;
    private String name;

    public Exercise() {
    }

    public Exercise(int courseId, String name) {
        this.courseId = courseId;
        this.name = name;
    }

    public Exercise(int id, int courseId, String name) {
        this.id = id;
        this.courseId = courseId;
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getCourseId() {
        return courseId;
    }

    public void setCourseId(int courseId) {
        this.courseId = courseId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Exercise exercise = (Exercise) o;
        return id == exercise.id && courseId == exercise.courseId;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, courseId);
    }
}
