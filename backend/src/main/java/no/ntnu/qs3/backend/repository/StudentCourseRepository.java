package no.ntnu.qs3.backend.repository;

import no.ntnu.qs3.backend.model.Course;
import no.ntnu.qs3.backend.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class StudentCourseRepository {

    @Autowired
    JdbcTemplate jdbcTemplate;

    public List<Course> findByStudId(int id) {

        String sql = "SELECT * FROM course WHERE id IN " +
                "(SELECT course_id FROM student_course WHERE stud_id = ?);";

        return jdbcTemplate.query(sql,
                BeanPropertyRowMapper.newInstance(Course.class), id);
    }

    public List<User> findByCourseId(int id) {

        String sql = "SELECT u.* FROM student s JOIN user u ON s.id = u.id WHERE s.id IN " +
                "(SELECT stud_id FROM student_course WHERE course_id = ?);";

        return jdbcTemplate.query(sql,
                BeanPropertyRowMapper.newInstance(User.class), id);
    }

    public int save(int courseId, int studId) {

        String sql = "INSERT INTO student_course (course_id, stud_id) VALUES (?,?);";

        return jdbcTemplate.update(sql, courseId, studId);
    }

    public int delete(int courseId, int studId) {

        String sql = "DELETE FROM student_course WHERE course_id = ? AND stud_id = ?;";

        return jdbcTemplate.update(sql, courseId, studId);
    }

    public boolean existsByStudIdAndCourseId(int studId, int courseId) {

        String sql = "SELECT COUNT(*) FROM student_course WHERE stud_id = ? AND course_id = ?;";

        int count = jdbcTemplate.queryForObject(sql, Integer.class, studId, courseId);

        return count > 0;
    }


}
