package no.ntnu.qs3.backend.service;


import no.ntnu.qs3.backend.model.Course;
import no.ntnu.qs3.backend.model.CourseRequest;
import no.ntnu.qs3.backend.model.CourseResponse;
import no.ntnu.qs3.backend.repository.CourseRepository;
import no.ntnu.qs3.backend.util.ModelConverter;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

import static no.ntnu.qs3.backend.util.ModelConverter.courseToResponse;
import static no.ntnu.qs3.backend.util.ModelConverter.requestToCourse;

/**
 * Service for handling courses.
 */
@Service
public class CourseService {

    private static final Logger logger = LogManager.getLogger(CourseService.class);

    @Autowired
    CourseRepository repo;

    /**
     * Creates a new course.
     * @param course to create.
     * @return created course.
     */
    public CourseResponse createNewCourse(CourseRequest course) {
        logger.debug("Creating course: {}" ,course);

        if (repo.existsByCodeAndYear(course.getCode(), course.getYear())) {
            logger.debug("Course already exists.");
            return null;
        }

        Course courseRef = requestToCourse(course);
        logger.debug("Saving {}...", course);

        Course newCourseRef = repo.save(courseRef);

        logger.debug("Created new course {}" ,newCourseRef);

        return courseToResponse(newCourseRef);
    }

    /**
     * Deletes a course.
     * @param id of course.
     * @return deleted course.
     * @throws RuntimeException if failed.
     */
    public CourseResponse deleteById(int id) throws RuntimeException {
        logger.debug("Deleting course {}...", id);
        Course course = repo.findById(id);

        if (course != null) {
            repo.deleteById(id);
            if (repo.existsById(id))
                throw new RuntimeException("Course was not deleted.");
            return courseToResponse(course);
        }
        return null;
    }

    /**
     * Gets course by id.
     * @param id of course.
     * @return the course.
     */
    public CourseResponse getById(int id) {
        Course course = repo.findById(id);
        logger.debug(course == null ? "Course not found." : "Found course {}", course);
        return course == null ? null : courseToResponse(course);
    }

    /**
     * Gets a all courses.
     * @return all courses.
     */
    public List<CourseResponse> getAll() {
        List<Course> courses = repo.findAll();
        logger.debug(courses.isEmpty() ? "No courses found." : courses.size() + " courses found.");
        return courses.isEmpty() ? null : courses.stream().map(ModelConverter::courseToResponse).collect(Collectors.toList());
    }
}
