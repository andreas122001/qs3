package no.ntnu.qs3.backend.service;

import no.ntnu.qs3.backend.model.*;
import no.ntnu.qs3.backend.repository.StudentCourseRepository;
import no.ntnu.qs3.backend.repository.StudentRepository;
import no.ntnu.qs3.backend.util.ModelConverter;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static no.ntnu.qs3.backend.util.ModelConverter.userRequestToResponse;

/**
 * A service that manages the link between student and course.
 * Primarily used by other services.
 */
@Service
public class StudentCourseService {

    private static final Logger logger = LogManager.getLogger(StudentCourseService.class);

    @Autowired
    private StudentCourseRepository repo;

    @Autowired
    private StudentService studentService;


    /**
     * Gets all students at a course.
     * @param id of course.
     * @return list of students.
     */
    public List<UserResponse> getStudentsAtCourse(int id) {
        List<User> students = repo.findByCourseId(id);
        logger.debug(students.isEmpty() ? "Found no students at course "+id : students.size() + " students found.");
        return students.stream().map(ModelConverter::userToResponse).collect(Collectors.toList());
    }


    /**
     * Adds students to a course.
     * @param courseId id of course.
     * @param students list of student info.
     * @return added students.
     */
    public List<UserResponse> addStudents(int courseId, List<UserRequest> students) {
        List<UserResponse> response = new ArrayList<>();
        for (UserRequest student : students) {
            response.add(addStudent(courseId, student));
            logger.debug("{} added to course.", student);
        }
        return response;
    }

    /**
     * Adds a student to a course. If student doesn't exist, create student.
     * @return added student.
     */
    public UserResponse addStudent(int courseId, UserRequest student) {
        UserResponse foundStudent = studentService.getByEmail(student.getEmail());

        // If user exists, add to student
        if (foundStudent != null) {
            logger.debug("Existing student found, adding to course...");
            // If exists, return null
            if (repo.existsByStudIdAndCourseId(foundStudent.getId(), courseId)){
                logger.debug("Student already exists in this course");
                return null;
            }
            // If not, add to course
            logger.debug("Adding existing student to course {}", courseId);
            if (repo.save(courseId, foundStudent.getId()) > 0)
                return userRequestToResponse(foundStudent.getId(), student);
            // If failed, return null
            else return null;
        }

        // If user does not exist, create user, add to student and add to course
        UserResponse newStudent = studentService.createNewStudent(student);
        logger.debug("Adding new student to course {}", courseId);
        if (repo.save(courseId, newStudent.getId()) > 0)
            return newStudent;

        // If nothing was done, return null
        return null;
    }


    /**
     * Gets courses of a specified student.
     * @param studId of student.
     * @return students courses.
     */
    public List<CourseResponse> getCoursesOfStudent(int studId) {
        logger.debug("Getting courses of student {}", studId);
        // Check if student exists.
        if (!studentService.existsById(studId)) {
            logger.debug("Student does not exist.");
            return null;
        }

        List<Course> courses = repo.findByStudId(studId);
        logger.debug(courses.isEmpty() ? "No courses found." : courses.size() + " courses found.");

        return courses.stream().map(ModelConverter::courseToResponse).collect(Collectors.toList());
    }

    /**
     * Removes a student from a course.
     * @param id of student.
     * @param student to remove.
     * @return removed student.
     */
    public UserResponse removeStudent(int id, UserRequest student) {

        UserResponse foundStudent = studentService.getByEmail(student.getEmail());

        if (foundStudent != null)
            if (repo.delete(id, foundStudent.getId()) > 0)
                return foundStudent;

        return null;
    }





}
