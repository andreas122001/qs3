package no.ntnu.qs3.backend.security;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.provisioning.InMemoryUserDetailsManager;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import org.springframework.web.filter.CorsFilter;


/**
 * Security configuration.
 * Authentication currently disabled - we had some problems...
 */
@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        // Https redirect
//        http.requiresChannel(channel ->
//                        channel.anyRequest().requiresSecure());

        // token endpoint is not protected
            http.cors().and().csrf().disable().headers().frameOptions().disable();
            http.authorizeRequests().antMatchers("/h2/**").permitAll();

//                .csrf().disable()
//                .cors().and()
//                .addFilterAfter(new JWTAuthorizationFilter(), UsernamePasswordAuthenticationFilter.class)
//                .authorizeRequests()
//                .antMatchers(HttpMethod.POST).permitAll()
//                .antMatchers(HttpMethod.GET).permitAll()
//                .antMatchers(HttpMethod.DELETE).permitAll()
//                .antMatchers(HttpMethod.PUT).permitAll()
//                .antMatchers(HttpMethod.POST, "/token").permitAll()
//                .antMatchers(HttpMethod.GET, "/swagger-ui/**", "/swagger-ui.html", "/webjars/**","/v2/**","/swagger-resources/**").permitAll()
//                .antMatchers(HttpMethod.GET, "/students/*/courses").permitAll()
//                .antMatchers(HttpMethod.GET,"/**").permitAll()
//                .antMatchers(HttpMethod.POST, "/**").permitAll()
//                .anyRequest().authenticated()
//                .anyRequest().permitAll();
    }


    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }


}
