package no.ntnu.qs3.backend.controller;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.security.Keys;
import no.ntnu.qs3.backend.model.LoginRequest;
import no.ntnu.qs3.backend.model.TokenResponse;
import no.ntnu.qs3.backend.model.User;
import no.ntnu.qs3.backend.repository.UserRepository;
import no.ntnu.qs3.backend.service.AdminService;
import no.ntnu.qs3.backend.service.StudasService;
import no.ntnu.qs3.backend.service.StudentService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.web.bind.annotation.*;

import java.security.Key;
import java.util.Date;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

/**
 * Controller for handling token authorization.
 * Modified example code from IDATT2105.
 */
@RestController
@RequestMapping(value = "/token")
@EnableAutoConfiguration
@CrossOrigin
public class TokenController {

    private static final Logger logger = LogManager.getLogger(TokenController.class);

    public static String keyStr = "C0cu9iM52zdIivMMzfICS5pNj2KIdOdZgR4NeUtHYsvIMF89Qv";

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private StudentService studentService;

    @Autowired
    private StudasService studasService;

    @Autowired
    private AdminService adminService;

    /**
     * Generates a token for a user given user info. Also applies user-roles to the token.
     * @param request user credentials.
     * @return the generated token and a list of authorities to be handled by the frontend application.
     * @throws Exception if token-generation fails.
     */
    @PostMapping(value = "")
    @ResponseStatus(value = HttpStatus.CREATED)
    public ResponseEntity<TokenResponse> requestGenerateToken(@RequestBody LoginRequest request) throws Exception {
        String email = request.getUsername();
        String password = request.getPassword();
        // check username and password are valid to access token
        logger.info("Requested token for email '{}, {}'", email, password);

        User user = userRepository.findByEmail(email);
        logger.debug(user==null ? "No user found." : "Found user '{}'", email);

        if (user != null && user.getPassword().equals(password)) {
            logger.info("Access granted for user '{}'.", email);
            StringBuilder authorities = new StringBuilder("USER");

            // Get authorities:
            authorities.append(studentService.existsById(user.getId()) ? ", STUDENT" : "");
            authorities.append(studasService.existsById(user.getId()) ? ", STUDAS" : "");
            authorities.append(adminService.existsById(user.getId()) ? ", ADMIN" : "");
            authorities.append(", ID(" + user.getId() + ")");
            logger.debug("Authorities: " + authorities);

            String token = generateToken(email, authorities.toString());
            return new ResponseEntity<>(new TokenResponse(token, authorities.toString()), HttpStatus.OK);
        }
        if (user != null && !user.getPassword().equals(password)) logger.debug("Password did not match username.");
        logger.info("Access denied for user '{}'.", email);
        return new ResponseEntity<>(null, HttpStatus.UNAUTHORIZED);
    }

    /**
     * Method for generating a token using JWT.
     * @param userId user id.
     * @param authorities the roles of the user. E.g. 'ADMIN'
     * @return the token.
     * @throws Exception if token-generation fails.
     */
    public String generateToken(String userId, String authorities) throws Exception {
        Key key = Keys.hmacShaKeyFor(keyStr.getBytes("UTF-8"));
        List<GrantedAuthority> grantedAuthorities = AuthorityUtils.commaSeparatedStringToAuthorityList(authorities);

        Claims claims = Jwts.claims().setSubject(userId);
        claims.put("userId", userId);
        claims.put("authorities", grantedAuthorities
                .stream()
                .map(GrantedAuthority::getAuthority)
                .collect(Collectors.toList()));

        return Jwts.builder()
                .setId(UUID.randomUUID().toString())
                .setSubject(userId)
                .setClaims(claims)
                .setIssuedAt(new Date(System.currentTimeMillis()))
                .setExpiration(new Date(System.currentTimeMillis() + 3600000))
                .signWith(key)
                .compact();
    }
}
