package no.ntnu.qs3.backend.controller;

import no.ntnu.qs3.backend.model.Course;
import no.ntnu.qs3.backend.repository.CourseRepository;
import no.ntnu.qs3.backend.repository.StudentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Ignore; Only for testing.
 */
@RestController
@CrossOrigin
public class TestController {

    @Autowired
    StudentRepository studentRepo;

    @Autowired
    CourseRepository subjectRepo;

    @GetMapping("/test")
    public String getTest() {
        return "GET TEST";
    }

    @PostMapping("/test")
    public String postTest() {
        return "POST TEST";
    }

    @PostMapping("/seed")
    public void seed() {
//        studentRepo.create(new Student("test@stud.no", "test","test","123"));
//        studentRepo.create(new Student("test2@stud.no", "test2","test2","123"));
//        studentRepo.create(new Student("test3@stud.no", "test3","test3","123"));

//        subjectRepo.save(new Course("Fullstack", "IDATT2105", "", 2021));
//        subjectRepo.save(new Course("Fullstack", "IDATT2105", "", 2022));
//        subjectRepo.save(new Course("Nettverk", "IDATT2106", "", 2021));
//        subjectRepo.save(new Course("Nettverk", "IDATT2106", "", 2022));
    }

}
