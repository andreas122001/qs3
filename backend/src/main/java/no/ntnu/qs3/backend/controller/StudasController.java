package no.ntnu.qs3.backend.controller;

import no.ntnu.qs3.backend.model.UserRequest;
import no.ntnu.qs3.backend.model.UserResponse;
import no.ntnu.qs3.backend.service.StudasService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

/**
 * Controller for handling requests tied to Student Assistants.
 */
@RestController
@RequestMapping("/studas")
@CrossOrigin
public class StudasController {

    private static final Logger logger = LogManager.getLogger(StudasController.class);

    @Autowired
    StudasService service;

    /**
     * Creates a new student assistant given user information. If user does not exist, it also creates the user.
     * @param request user information.
     * @return the created student assistant as user info.
     */
    @PostMapping("")
    public ResponseEntity<UserResponse> addStudas(@RequestBody UserRequest request) {
        logger.info("Requested adding studas: {}", request);
        UserResponse newStudas = service.createNewStudas(request);
        if (newStudas != null)
            return new ResponseEntity<>(newStudas, HttpStatus.CREATED);
        logger.debug("Studas not added.");
        return new ResponseEntity<>(HttpStatus.CONFLICT);
    }

    /**
     * Gets a student assistant given user-id.
     * @param id of user.
     * @return user of the student assistant.
     */
    @GetMapping("/{id}")
    public ResponseEntity<UserResponse> getById(@PathVariable("id") int id) {
        logger.info("Requested studas by id: " + id);
        UserResponse studas = service.getById(id);
        if (studas != null)
            return new ResponseEntity<>(studas, HttpStatus.OK);
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    /**
     * Deletes a student assistant from the database.
     * @param id of user.
     * @return the deleted student assistant as user response.
     */
    @DeleteMapping("/{id}")
    @ResponseStatus
    public ResponseEntity<UserResponse> removeStudas(@PathVariable("id") int id) {
        logger.info("Requested remove studas with id: {}", id);
        UserResponse user = service.removeById(id);
        if (user != null)
            return new ResponseEntity<>(user, HttpStatus.OK);
        logger.warn("Studas not removed.");
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

}
