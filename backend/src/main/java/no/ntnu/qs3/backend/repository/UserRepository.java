package no.ntnu.qs3.backend.repository;

import no.ntnu.qs3.backend.model.Course;
import no.ntnu.qs3.backend.model.User;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface UserRepository extends CrudRepository<User, Integer> {

    User findByEmail(String email);

    User findById(int id);

    List<User> findAll();

    boolean existsByEmail(String email);


}
