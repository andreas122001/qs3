package no.ntnu.qs3.backend.repository;

import no.ntnu.qs3.backend.model.Course;
import no.ntnu.qs3.backend.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class StudasCourseRepository {

    @Autowired
    JdbcTemplate jdbcTemplate;

    public List<Course> findByStudasId(int id) {

        String sql = "SELECT * FROM course WHERE id IN " +
                "(SELECT course_id FROM studas_course WHERE studas_id = ?);";

        return jdbcTemplate.query(sql,
                BeanPropertyRowMapper.newInstance(Course.class), id);
    }

    public List<User> findByCourseId(int id) {

        String sql = "SELECT u.* FROM studas s JOIN user u ON s.id = u.id WHERE s.id IN " +
                "(SELECT studas_id FROM studas_course WHERE course_id = ?);";

        return jdbcTemplate.query(sql,
                BeanPropertyRowMapper.newInstance(User.class), id);
    }

    public int save(int courseId, int studasId) {

        String sql = "INSERT INTO studas_course (course_id, studas_id) VALUES (?,?);";

        return jdbcTemplate.update(sql, courseId, studasId);
    }

    public int delete(int courseId, int studasId) {

        String sql = "DELETE FROM studas_course WHERE course_id = ? AND studas_id = ?;";

        return jdbcTemplate.update(sql, courseId, studasId);
    }

    public boolean existsByStudasIdAndCourseId(int studasId, int courseId) {

        String sql = "SELECT COUNT(*) FROM studas_course WHERE studas_id = ? AND course_id = ?;";

        int count = jdbcTemplate.queryForObject(sql, Integer.class, studasId, courseId);

        return count > 0;
    }

}
