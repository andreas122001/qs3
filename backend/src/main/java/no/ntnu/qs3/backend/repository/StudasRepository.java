package no.ntnu.qs3.backend.repository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

@Repository
public class StudasRepository {

    @Autowired
    JdbcTemplate jdbcTemplate;

    public boolean existsById(int id) {
        String sql = "SELECT COUNT(*) FROM studas WHERE id = ?;";

        int count = jdbcTemplate.queryForObject(sql,
                Integer.class,id);

        return count > 0;
    }

    public boolean deleteById(int id) {
        String sql = "DELETE FROM studas WHERE id = ?;";

        return jdbcTemplate.update(sql, id) > 0;

    }

    public boolean save(int id) {
        String sql = "INSERT INTO studas (id) VALUES (?);";

        return jdbcTemplate.update(sql,id) > 0;
    }

}
