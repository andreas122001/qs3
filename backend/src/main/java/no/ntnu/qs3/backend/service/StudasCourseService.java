package no.ntnu.qs3.backend.service;

import no.ntnu.qs3.backend.model.*;
import no.ntnu.qs3.backend.repository.StudasCourseRepository;
import no.ntnu.qs3.backend.util.ModelConverter;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static no.ntnu.qs3.backend.util.ModelConverter.userRequestToResponse;

@Service
public class StudasCourseService {

    private static final Logger logger = LogManager.getLogger(StudasCourseService.class);

    @Autowired
    StudasCourseRepository repo;

    @Autowired
    private StudasService studasService;


    /**
     * Gets all student assistants at a course.
     * @param id of course.
     * @return list of student assistants.
     */
    public List<UserResponse> getStudasesAtCourse(int id) {
        List<User> studases = repo.findByCourseId(id);
        logger.debug(studases.isEmpty() ? "Found no studases at course " + id : studases.size() + " studases found.");
        return studases.stream().map(ModelConverter::userToResponse).collect(Collectors.toList());
    }

    /**
     * Adds a list of student assistants to a course.
     * @param courseId id of course.
     * @param studases list of users to add.
     * @return list of added users.
     */
    public List<UserResponse> addStudases(int courseId, List<UserRequest> studases) {
        List<UserResponse> response = new ArrayList<>();
        for (UserRequest studas : studases) {
            response.add(addStudas(courseId, studas));
            logger.debug("{} added to course.", studas);
        }
        return response;
    }


    /**
     * Adds a student assistant to a course. If assistant does not exist, it creates a new student assistant.
     * @param courseId id of course.
     * @param studas user info.
     * @return added student assistant.
     */
    public UserResponse addStudas(int courseId, UserRequest studas) {
        UserResponse foundStudas = studasService.getByEmail(studas.getEmail());

        // If user exists, add to studas
        if (foundStudas != null) {
            logger.debug("Existing studas found, adding to course...");
            // If exists, return null
            if (repo.existsByStudasIdAndCourseId(foundStudas.getId(), courseId)) {
                logger.debug("Studas already exists in this course");
                return null;
            }
            // If not, add to course
            if (repo.save(courseId, foundStudas.getId()) > 0)
                return userRequestToResponse(foundStudas.getId(), studas);
                // If failed, return null
            else return null;
        }

        // If user does not exist, create user, add to studas and add to course
        UserResponse newStudent = studasService.createNewStudas(studas);
        if (repo.save(courseId, newStudent.getId()) > 0)
            return newStudent;

        // If nothing was done, return null
        return null;
    }


    /**
     * Gets the courses of a student assistant.
     * @param studasId id of student assistant.
     * @return list of courses.
     */
    public List<CourseResponse> getCoursesOfStudas(int studasId) {
        logger.debug("Getting courses of studas {}", studasId);
        // Check if studas exists.
        if (!studasService.existsById(studasId)) {
            logger.debug("Studas does not exist.");
            return null;
        }

        List<Course> courses = repo.findByStudasId(studasId);
        logger.debug(courses.isEmpty() ? "No courses found." : courses.size() + " courses found.");

        return courses.stream().map(ModelConverter::courseToResponse).collect(Collectors.toList());
    }


    /**
     * Removes a student assistant from a course.
     * @param id of course.
     * @param studas user to remove.
     * @return removed user.
     */
    public UserResponse removeStudas(int id, UserRequest studas) {

        UserResponse foundStudas = studasService.getByEmail(studas.getEmail());

        if (foundStudas != null)
            if (repo.delete(id, foundStudas.getId()) > 0)
                return foundStudas;

        return null;
    }

}
