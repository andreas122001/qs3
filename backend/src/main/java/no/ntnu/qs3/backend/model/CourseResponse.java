package no.ntnu.qs3.backend.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Objects;

/**
 * A course as JSON.
 */
public class CourseResponse {

    private int id;
    private String name;
    private String code;
    private String description;
    private int year;

    @JsonCreator
    public CourseResponse(@JsonProperty("id") int id,
                          @JsonProperty("name") String name,
                          @JsonProperty("code") String code,
                          @JsonProperty("description") String description,
                          @JsonProperty("year") int year) {
        this.id = id;
        this.name = name;
        this.code = code;
        this.description = description;
        this.year = year;
    }

    @JsonProperty("id")
    public int getId() {
        return id;
    }

    @JsonProperty("id")
    public void setId(int id) {
        this.id = id;
    }

    @JsonProperty("year")
    public int getYear() {
        return year;
    }

    @JsonProperty("year")
    public void setYear(int year) {
        this.year = year;
    }

    @JsonProperty("name")
    public String getName() {
        return name;
    }

    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    @JsonProperty("code")
    public String getCode() {
        return code;
    }

    @JsonProperty("code")
    public void setCode(String code) {
        this.code = code;
    }

    @JsonProperty("description")
    public String getDescription() {
        return description;
    }

    @JsonProperty("description")
    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return "'" + code + " [" + year + "]'";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CourseResponse course = (CourseResponse) o;
        return (year == course.year && Objects.equals(code, course.code)) || id == course.id;
    }

    @Override
    public int hashCode() {
        return Objects.hash(year, code);
    }


}
