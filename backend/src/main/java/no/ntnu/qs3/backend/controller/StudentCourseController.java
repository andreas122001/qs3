package no.ntnu.qs3.backend.controller;

import no.ntnu.qs3.backend.model.CourseResponse;
import no.ntnu.qs3.backend.model.UserRequest;
import no.ntnu.qs3.backend.model.UserResponse;
import no.ntnu.qs3.backend.service.StudentCourseService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Contains endpoints for registering/removing students to/from courses. Also for getting courses of a student.
 */
@RestController
@RequestMapping("")
@CrossOrigin
public class StudentCourseController {

    private static final Logger logger = LogManager.getLogger(StudentCourseController.class);

    @Autowired
    StudentCourseService service;


    /**
     * Gets the students at a specific course.
     * @param id of course.
     * @return list of students at course
     */
    @GetMapping(value = "/courses/{id}/students")
    public ResponseEntity<List<UserResponse>> getStudents(@PathVariable("id") int id) {

        logger.info("Requested students on course id {}", id);

        List<UserResponse> students = service.getStudentsAtCourse(id);

        if (students != null)
            return new ResponseEntity<>(students, HttpStatus.OK);
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    /** TODO: ADMIN
     * Adds a list of students to the course.
     * @param id of course.
     * @param request list of students.
     * @return list of added students.
     */
    @PostMapping("/courses/{id}/students/list")
    public ResponseEntity<List<UserResponse>> addStudents(@PathVariable("id") int id,
                                                          @RequestBody List<UserRequest> request) {
        logger.info("Requested adding students: {}", request.size());
        List<UserResponse> newStudents = service.addStudents(id, request);
        if (newStudents != null)
            return new ResponseEntity<>(newStudents, HttpStatus.CREATED);
        logger.debug("Student not added.");
        return new ResponseEntity<>(HttpStatus.CONFLICT);
    }



    /** TODO: ADMIN
     * Adds a student to a course. Handled by StudentService.
     * @param id of course.
     * @param student student to add.
     * @return added student.
     */
    @PostMapping(value = "/courses/{id}/students", consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<UserResponse> addStudent(@PathVariable("id") int id,
                                                  @RequestBody UserRequest student) {

        logger.info("Requested adding student {} to course {}", student, id);
        UserResponse newStudent = service.addStudent(id, student);
        if (newStudent != null) {
            logger.info("Student was added to the course");
            return new ResponseEntity<>(newStudent, HttpStatus.CREATED);
        }
        logger.warn("Student not added.");
        return new ResponseEntity<>(HttpStatus.CONFLICT);
    }


    /**
     * Deletes a student from given course.
     * @param id of course
     * @return deleted object.
     */
    @DeleteMapping("/courses/{id}/students")
    public ResponseEntity<UserResponse> deleteStudent(@PathVariable("id") int id, @RequestBody UserRequest student) {

        logger.info("Requested deleting student from course {}", id);
        UserResponse deletedStudent = service.removeStudent(id, student);

        if (deletedStudent != null)
            return new ResponseEntity<>(deletedStudent, HttpStatus.OK);
        else return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }


    /**
     * Gets the courses of a student.
     * @param id of student.
     * @return students courses.
     */
    @GetMapping("students/{id}/courses")
    public ResponseEntity<List<CourseResponse>> getCourses(@PathVariable("id") int id) {

        logger.info("Requested courses for student id " + id);
        List<CourseResponse> courses = service.getCoursesOfStudent(id);

        if (courses != null) {
            return new ResponseEntity<>(courses, HttpStatus.OK);
        }
        else return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

}
