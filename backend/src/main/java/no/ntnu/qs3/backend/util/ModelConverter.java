package no.ntnu.qs3.backend.util;

import no.ntnu.qs3.backend.model.*;

/**
 * Util for converting between database-model, JSON-request and JSON-response.
 */
public class ModelConverter {

    public static UserResponse userToResponse(User user) {
        return new UserResponse(user.getId(), user.getEmail(), user.getLastName(), user.getFirstName());
    }

    public static UserResponse userRequestToResponse(int id, UserRequest user) {
        return new UserResponse(id, user.getEmail(), user.getLastname(), user.getFirstname());
    }

    public static CourseResponse courseToResponse(Course course) {
        return new CourseResponse(course.getId(), course.getName(), course.getCode(), course.getDescription(), course.getYear());
    }

    public static Course requestToCourse(CourseRequest request) {
        return new Course(request.getName(), request.getCode(), request.getDescription(), request.getYear());
    }

    public static ExerciseResponse exerciseToResponse(Exercise exercise) {
        return new ExerciseResponse(exercise.getId(), exercise.getCourseId(), exercise.getName());
    }

}
