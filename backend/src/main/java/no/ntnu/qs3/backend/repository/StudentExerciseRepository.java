package no.ntnu.qs3.backend.repository;

import no.ntnu.qs3.backend.model.Exercise;
import no.ntnu.qs3.backend.model.ExerciseResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class StudentExerciseRepository {

    @Autowired
    JdbcTemplate jdbcTemplate;

    public List<Exercise> findAllByStudIdAndCourseId(int studId, int courseId) {
        String sql = "SELECT * FROM exercise WHERE course_id = ? AND id IN " +
                "(SELECT exercise_id FROM student_exercise WHERE stud_id = ?);";

        return jdbcTemplate.query(sql,
                new BeanPropertyRowMapper<>(Exercise.class), courseId, studId);
    }

    public boolean save(int studId, int exerciseId) {
        String sql = "INSERT INTO student_exercise (stud_id, exercise_id) " +
                "VALUES (?, ?);";

        return jdbcTemplate.update(sql, studId, exerciseId) > 0;
    }

    public boolean deleteByStudIdAndExerciseId(int studId, int exerciseId) {
        String sql = "DELETE FROM student_exercise WHERE stud_id = ? AND exercise_id = ?;";

        return jdbcTemplate.update(sql, studId, exerciseId) > 0;
    }



}
