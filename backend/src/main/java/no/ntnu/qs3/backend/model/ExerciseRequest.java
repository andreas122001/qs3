package no.ntnu.qs3.backend.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Objects;

/**
 * Exercise as JSON
 */
public class ExerciseRequest {

    private String name;

    public ExerciseRequest(@JsonProperty("name") String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return name;
    }

    @JsonProperty("name")
    public String getName() {
        return name;
    }

    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ExerciseRequest that = (ExerciseRequest) o;
        return Objects.equals(name, that.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name);
    }
}
