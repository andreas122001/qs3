package no.ntnu.qs3.backend.repository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

@Repository
public class AdminRepository {

    @Autowired
    JdbcTemplate jdbcTemplate;

    public boolean existsById(int id) {
        String sql = "SELECT COUNT(*) FROM admin WHERE id = ?;";

        int count = jdbcTemplate.queryForObject(sql,
                Integer.class,id);

        return count > 0;
    }

    public boolean deleteById(int id) {
        String sql = "DELETE FROM admin WHERE id = ?;";

        return jdbcTemplate.update(sql, id) > 0;

    }

    public boolean save(int id) {
        String sql = "INSERT INTO admin (id) VALUES (?);";

        return jdbcTemplate.update(sql,id) > 0;
    }

}
