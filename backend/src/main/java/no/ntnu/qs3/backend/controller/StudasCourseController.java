package no.ntnu.qs3.backend.controller;

import no.ntnu.qs3.backend.model.CourseResponse;
import no.ntnu.qs3.backend.model.UserRequest;
import no.ntnu.qs3.backend.model.UserResponse;
import no.ntnu.qs3.backend.service.StudasCourseService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Controls interactions between student assistants and courses.
 * Can add/delete/get students assistant at a course and get all courses a student assistant is a part of.
 */
@RestController
@RequestMapping("")
@CrossOrigin
public class StudasCourseController {

    private static final Logger logger = LogManager.getLogger(StudasCourseController.class);

    @Autowired
    StudasCourseService service;

    /**
     * Gets all student assistants at a course.
     * @param id of course.
     * @return list of student assistants.
     */
    @GetMapping(value = "/courses/{id}/studas")
    public ResponseEntity<List<UserResponse>> getStudasesOnCourse(@PathVariable("id") int id) {

        logger.info("Requested studases on course id {}", id);

        List<UserResponse> users = service.getStudasesAtCourse(id);

        if (users != null)
            return new ResponseEntity<>(users, HttpStatus.OK);
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    /**
     * Adds a student assistant to a course, given user-info.
     * If student assistant does not exist, it will create a new student assistant from the data.
     * @param id of course.
     * @param studas user info.
     * @return added student assistant.
     */
    @PostMapping(value = "/courses/{id}/studas", consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<UserResponse> addStudas(@PathVariable("id") int id,
                                                  @RequestBody UserRequest studas) {

        logger.info("Requested adding studas {} to course {}", studas, id);
        UserResponse newStudas = service.addStudas(id, studas);
        if (newStudas != null) {
            logger.info("Studas was added to the course");
            return new ResponseEntity<>(newStudas, HttpStatus.CREATED);
        }
        logger.warn("Studas not added.");
        return new ResponseEntity<>(HttpStatus.CONFLICT);
    }


    /** TODO: ADMIN
     * Adds a list of studases to the course.
     * @param id of course.
     * @param request list of studases.
     * @return list of added studases.
     */
    @PostMapping("/courses/{id}/studas/list")
    public ResponseEntity<List<UserResponse>> addStudases(@PathVariable("id") int id,
                                                          @RequestBody List<UserRequest> request) {
        logger.info("Requested adding {} studases to course with id {}", request.size(), id);
        List<UserResponse> newStudases = service.addStudases(id, request);
        if (newStudases != null)
            return new ResponseEntity<>(newStudases, HttpStatus.CREATED);
        logger.debug("Studases not added.");
        return new ResponseEntity<>(HttpStatus.CONFLICT);
    }


    /**
     * Removes a student assistant from a course.
     * @param id of course
     * @param studas user info.
     * @return removed student assistant.
     */
    @DeleteMapping("/courses/{id}/studas")
    public ResponseEntity<UserResponse> deleteStudas(@PathVariable("id") int id,
                                                     @RequestBody UserRequest studas) {

        logger.info("Requested deleting studas from course {}", id);
        UserResponse deletedStudas = service.removeStudas(id, studas);

        if (deletedStudas != null)
            return new ResponseEntity<>(deletedStudas, HttpStatus.OK);
        else return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }


    /**
     * Gets all courses of a student assistant.
     * @param id of student assistant.
     * @return list of courses.
     */
    @GetMapping("studas/{id}/courses")
    public ResponseEntity<List<CourseResponse>> getCourses(@PathVariable("id") int id) {

        logger.info("Requested courses for studas id " + id);
        List<CourseResponse> courses = service.getCoursesOfStudas(id);

        if (courses != null) {
            return new ResponseEntity<>(courses, HttpStatus.OK);
        }
        else return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

}
