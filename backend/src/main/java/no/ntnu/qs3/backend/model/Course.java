package no.ntnu.qs3.backend.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.springframework.data.annotation.Id;

import java.util.Objects;

/**
 * A model-class of a subject in university.
 */
public class Course {

    @Id
    private int id;
    private int year;
    private String code;
    private String name;
    private String description;

    public Course() {
    }

    public Course(String name, String code, String description, int year) {
        this.name = name;
        this.code = code;
        this.description = description;
        this.year = year;
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return "'"+ id +"-" + code + " [" + year + "]'";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Course subject = (Course) o;
        return year == subject.year && Objects.equals(code, subject.code);
    }

    @Override
    public int hashCode() {
        return Objects.hash(year, code);
    }
}
