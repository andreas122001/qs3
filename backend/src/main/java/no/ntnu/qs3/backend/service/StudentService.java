package no.ntnu.qs3.backend.service;

import no.ntnu.qs3.backend.model.UserRequest;
import no.ntnu.qs3.backend.model.UserResponse;
import no.ntnu.qs3.backend.repository.StudentRepository;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * A service that handles editing the student-table in the database.
 */
@Service
public class StudentService {

    private static final Logger logger = LogManager.getLogger(StudentService.class);

    @Autowired
    StudentRepository repo;

    @Autowired
    UserService userService;

    /**
     * Finds a student by id, and returns user-data for this student.
     * Uses user-service to find user-data.
     * @param id of student.
     * @return user data as StudentJson.
     */
    public UserResponse getById(int id) {
        logger.debug("Finding student by id " + id + "...");
        UserResponse student = null;

        // If student exists, find associated user in user repo
        if (repo.existsById(id)) {
            student = userService.getById(id);
            logger.debug("Found student: " + student);
        } else logger.debug("Student not found.");

        return student;
    }


    // TODO: ADMIN
    /**
     * Removes a user from the student table. The user still persists.
     * @param id of student.
     * @return true if successful.
     */
    public UserResponse removeById(int id) {
        UserResponse user = userService.getById(id);

        if (user != null) {
            if (repo.deleteById(id))
                return user;
        }
        return null;
    }

    /**
     * Finds a student given a email.
     * @param email of user.
     * @return student.
     */
    public UserResponse getByEmail(String email) {
        UserResponse user = userService.getByEmail(email);

        if (user != null)
            if (repo.existsById(user.getId()))
                return user;

        return null;
    }

    /**
     * Checks if a student exists by id.
     * @param id of student.
     * @return true/false.
     */
    public boolean existsById(int id) {
        return repo.existsById(id);
    }


    /**
     * Checks if a student exits by email.
     * @param email to check with.
     * @return true/false.
     */
    public boolean existsByEmail(String email) {
        UserResponse user = userService.getByEmail(email);

        if (user != null) {
            return repo.existsById(user.getId());
        }
        return false;
    }


    /**
     * Creates a new student. If user does not exist, creates a new user.
     * @param student user info.
     * @return created student.
     */
    public UserResponse createNewStudent(UserRequest student) {

        logger.debug("Creating student: {}", student);

        // If exists, return null
        if (existsByEmail(student.getEmail())) {
            logger.debug("Student already exists.");
            return null;
        }

        // If user exists
        UserResponse user = userService.getByEmail(student.getEmail());
        if (user != null) {
            logger.debug("User exists, adding to student.");
            repo.save(user.getId());
            return user;
        }

        // If user does not exist, create user and add student
        logger.debug("User does not exist, creating user...");
        UserResponse newUser = userService.createNewUser(student);
        logger.debug("Adding new user '{}' to student.", newUser);
        repo.save(newUser.getId());

        return newUser;
    }







}
