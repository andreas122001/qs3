package no.ntnu.qs3.backend.controller;

import no.ntnu.qs3.backend.model.ExerciseRequest;
import no.ntnu.qs3.backend.model.ExerciseResponse;
import no.ntnu.qs3.backend.service.ExerciseService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Controller for adding, getting and deleting exercises from a course.
 */
@RestController
@RequestMapping("")
@CrossOrigin
public class ExerciseController {

    private static final Logger logger = LogManager.getLogger(ExerciseController.class);

    @Autowired
    private ExerciseService service;

    /**
     * Gets all exercises of a course.
     * @param id of course.
     * @return exercises.
     */
    @GetMapping("/courses/{id}/exercises")
    public ResponseEntity<List<ExerciseResponse>> getExercisesAtCourse(@PathVariable("id") int id) {
        logger.info("Requested exercises of course with id {}", id);
        List<ExerciseResponse> response = service.getExercisesByCourseId(id);

        if (response != null)
            return new ResponseEntity<>(response, HttpStatus.OK);
        else return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    /**
     * Adds a exercise to a course.
     * @param id of course.
     * @param request exercise to add.
     * @return created exercise.
     */
    @PostMapping("/courses/{id}/exercises")
    public ResponseEntity<ExerciseResponse> addExerciseToCourse(@PathVariable("id") int id,
                                                @RequestBody ExerciseRequest request) {
        logger.info("Requested adding exercise '{}' to course with id {}" ,request, id);
        ExerciseResponse response = service.createNewExerciseAtCourse(id, request);

        if (response != null)
            return new ResponseEntity<>(response, HttpStatus.CREATED);
        else return new ResponseEntity<>(HttpStatus.CONFLICT);
    }


    /**
     * Gets a exercise by id of course and id of exercise.
     * @param courseId id of course.
     * @param exerciseId if of exercise.
     * @return the exercise.
     */
    @GetMapping("/courses/{course_id}/exercises/{exercise_id}")
    public ResponseEntity<ExerciseResponse> getExerciseById(@PathVariable("course_id") int courseId,
                                            @PathVariable("exercise_id") int exerciseId) {
        logger.info("Requested exercise with id {} at course with id {}", exerciseId, courseId);
        ExerciseResponse response = service.getExerciseAtCourseById(courseId, exerciseId);

        if (response != null)
            return new ResponseEntity<>(response, HttpStatus.OK);
        else return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }


    /**
     * Deletes a exercise from a course.
     * @param courseId id of course.
     * @param exerciseId if of exercise.
     * @return deleted exercise.
     */
    @DeleteMapping("/courses/{course_id}/exercises/{exercise_id}")
    public ResponseEntity<ExerciseResponse> deleteExerciseById(@PathVariable("course_id") int courseId,
                                            @PathVariable("exercise_id") int exerciseId) {

        ExerciseResponse response = service.deleteExerciseAtCourseById(courseId, exerciseId);

        if (response != null)
            return new ResponseEntity<>(response, HttpStatus.OK);
        else return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    /**
     * Gets a students completed exercises at a course.
     * @param studId id of student.
     * @param courseId id of course.
     * @return students completed exercises.
     */
    @GetMapping("/students/{student_id}/courses/{course_id}/exercises")
    public ResponseEntity<List<ExerciseResponse>> getStudentCompletedExercises(@PathVariable("student_id") int studId,
                                                                               @PathVariable("course_id") int courseId) {
        logger.info("Requested completed exercises of student id {} at course id {}", studId, courseId);
        List<ExerciseResponse> exercises = service.getStudentCompletedExercises(studId, courseId);

        if (exercises != null)
            return new ResponseEntity<>(exercises, HttpStatus.OK);
        else return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    /**
     * Adds an exercise to a students completed exercises.
     * @param studId id of student.
     * @param exerciseId id of exercise.
     * @return added exercise.
     */
    @PostMapping("/students/{student_id}/exercises/{exercise_id}")
    public ResponseEntity<ExerciseResponse> addExerciseToStudentCompleted(@PathVariable("student_id") int studId,
                                                                          @PathVariable("exercise_id") int exerciseId) {
        logger.info("Requested completing exercise id {} of student id {}", exerciseId, studId);
        ExerciseResponse response = service.addCompletedExerciseToStudent(studId, exerciseId);

        if (response != null)
            return new ResponseEntity<>(response, HttpStatus.OK);
        else return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    /**
     * Deletes an exercise from a students completed exercises.
     * @param studId id of student.
     * @param exerciseId id of exercise.
     * @return deleted exercise.
     */
    @DeleteMapping("/students/{student_id}/exercises/{exercise_id}")
    public ResponseEntity<ExerciseResponse> deleteExerciseFromStudentCompleted(@PathVariable("student_id") int studId,
                                                                          @PathVariable("exercise_id") int exerciseId) {
        logger.info("Requested deleting completion of exercise id {} of student id {}", exerciseId, studId);
        ExerciseResponse response = service.deleteCompletedExerciseFromStudent(studId, exerciseId);

        if (response != null)
            return new ResponseEntity<>(response, HttpStatus.OK);
        else return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

}
