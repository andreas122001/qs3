package no.ntnu.qs3.backend.controller;

import no.ntnu.qs3.backend.BackendApplication;
import no.ntnu.qs3.backend.model.UserRequest;
import no.ntnu.qs3.backend.model.UserResponse;
import no.ntnu.qs3.backend.service.UserService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;

import static no.ntnu.qs3.backend.util.ModelConverter.userRequestToResponse;
import static no.ntnu.qs3.backend.util.TestUtil.asJsonString;
import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.when;
import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.MOCK;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ActiveProfiles("test")
@SpringBootTest(webEnvironment = MOCK, classes = BackendApplication.class)
@AutoConfigureMockMvc(addFilters = false)
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.ANY)
public class UserControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private UserService service;

    private UserRequest request = new UserRequest("test@test.no",
            "test", "test");
    private UserResponse response = userRequestToResponse(1, request);

    @Test
    void successfulPostReturnsExpected() throws Exception {
        when(service.createNewUser(request))
                .thenReturn(response);

        mockMvc.perform(post("/users")
                    .content(asJsonString(request))
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.email", is("test@test.no")))
                .andExpect(jsonPath("$.firstname", is("test")))
                .andExpect(jsonPath("$.lastname", is("test")));
    }

    @Test
    void getNonExistentReturns404() throws Exception {
        mockMvc.perform(get("/users/{id}",1)
                    .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound())
                .andExpect(jsonPath("$").doesNotExist());
    }

    @Test
    void gettingExistentReturnsExpected() throws Exception {
        when(service.getById(1))
                .thenReturn(response);

        mockMvc.perform(get("/users/{id}",1)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.email", is("test@test.no")))
                .andExpect(jsonPath("$.firstname", is("test")))
                .andExpect(jsonPath("$.lastname", is("test")));
    }
}
