package no.ntnu.qs3.backend.service;

import no.ntnu.qs3.backend.BackendApplication;
import no.ntnu.qs3.backend.model.CourseRequest;
import no.ntnu.qs3.backend.model.CourseResponse;
import org.junit.jupiter.api.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@ActiveProfiles("test")
@SpringBootTest(classes = BackendApplication.class)
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_CLASS)
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.ANY)
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class CourseServiceIntegrationTest {

    @Autowired
    CourseService service;

    CourseRequest request;
    CourseRequest request2;

    @BeforeEach
    void setup() {
        request = new CourseRequest("Teststack", "IDATT6969", "desc", 1917);
        request2 = new CourseRequest("Nett test", "IDATT9680", "desc", 1914);
    }

    @Order(1)
    @Test
    void addingCourse_works() {
        CourseResponse response = service.createNewCourse(request);

        assertEquals(1, response.getId());

        assertEquals(request.getName(), response.getName());
        assertEquals(request.getCode(), response.getCode());
        assertEquals(request.getDescription(), response.getDescription());
        assertEquals(request.getYear(), response.getYear());
    }

    @Order(2)
    @Test
    void addingCourse_whenOneExists_incrementsId() {

        CourseResponse response = service.createNewCourse(request2);

        assertEquals(2, response.getId());

        assertEquals(request2.getName(), response.getName());
        assertEquals(request2.getCode(), response.getCode());
        assertEquals(request2.getDescription(), response.getDescription());
        assertEquals(request2.getYear(), response.getYear());
    }

    @Order(3)
    @Test
    void getById_works() {
        CourseResponse response = service.getById(1);

        assertEquals(1, response.getId());

        assertEquals(request.getName(), response.getName());
        assertEquals(request.getCode(), response.getCode());
        assertEquals(request.getDescription(), response.getDescription());
        assertEquals(request.getYear(), response.getYear());
    }

    @Order(4)
    @Test
    void getAll_returnsAll() {
        assertDoesNotThrow(service::getAll);

        List<CourseResponse> response = service.getAll();
        assertEquals(2, response.size());
    }

    @Order(5)
    @Test
    void deletingCourse_works() {
        CourseResponse response = service.deleteById(1);

        assertEquals(1, response.getId());

        assertEquals(request.getName(), response.getName());
        assertEquals(request.getCode(), response.getCode());
        assertEquals(request.getDescription(), response.getDescription());
        assertEquals(request.getYear(), response.getYear());

        response = service.getById(1);

        assertNull(response);
    }






}
