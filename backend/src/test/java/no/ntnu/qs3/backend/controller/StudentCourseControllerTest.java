package no.ntnu.qs3.backend.controller;

import no.ntnu.qs3.backend.BackendApplication;
import no.ntnu.qs3.backend.model.Course;
import no.ntnu.qs3.backend.model.CourseResponse;
import no.ntnu.qs3.backend.model.UserRequest;
import no.ntnu.qs3.backend.model.UserResponse;
import no.ntnu.qs3.backend.service.StudentCourseService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;

import java.util.ArrayList;
import java.util.List;

import static no.ntnu.qs3.backend.util.ModelConverter.courseToResponse;
import static no.ntnu.qs3.backend.util.ModelConverter.userRequestToResponse;
import static no.ntnu.qs3.backend.util.TestUtil.asJsonString;
import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.when;
import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.MOCK;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ActiveProfiles("test")
@SpringBootTest(webEnvironment = MOCK, classes = BackendApplication.class)
@AutoConfigureMockMvc(addFilters = false)
public class StudentCourseControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private StudentCourseService service;

    UserRequest studentReq;
    UserResponse studentRes;
    Course course;

    List<UserResponse> students;
    List<CourseResponse> courses;

    @BeforeEach
    void setup() {
        studentReq = new UserRequest("Test", "test", "test");
        studentRes = userRequestToResponse(1, studentReq);
        course = new Course("Test", "test", "test" ,1972);

        students = new ArrayList<>();
        courses = new ArrayList<>();

        students.add(studentRes);
        courses.add(courseToResponse(course));
    }

    @Test
    void gettingStudentsReturnsExpected() throws Exception {
        when(service.getStudentsAtCourse(1))
                .thenReturn(students);

        mockMvc.perform(get("/courses/{id}/students",1))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[0].email", is(studentReq.getEmail())));
    }

    @Test
    void gettingCoursesReturnsExpected() throws Exception {
        when(service.getCoursesOfStudent(1))
                .thenReturn(courses);

        mockMvc.perform(get("/students/1/courses"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[0].code", is(course.getCode())));
    }

    @Test
    void addStudentReturnsExpected() throws Exception {
        when(service.addStudent(1, studentReq))
                .thenReturn(userRequestToResponse(1, studentReq));

        mockMvc.perform(post("/courses/1/students")
                        .content(asJsonString(studentReq))
                        .contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.email", is(studentReq.getEmail())))
                .andExpect(jsonPath("$.id", is(1)));
    }

    @Test
    void deleteExistentStudentReturnsExpected() throws Exception {
        when(service.removeStudent(1, studentReq))
                .thenReturn(studentRes);

        mockMvc.perform(delete("/courses/1/students")
                        .content(asJsonString(studentReq))
                        .contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.email", is(studentReq.getEmail())));
    }

    @Test
    void deleteNonExistentReturns404() throws Exception {
        mockMvc.perform(delete("/courses/1/students")
                        .content(asJsonString(studentReq))
                        .contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isNotFound())
                .andExpect(jsonPath("$").doesNotExist());
    }
}
