package no.ntnu.qs3.backend.service;

import no.ntnu.qs3.backend.BackendApplication;
import no.ntnu.qs3.backend.model.UserRequest;
import no.ntnu.qs3.backend.model.UserResponse;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

@ActiveProfiles("test")
@SpringBootTest(classes = BackendApplication.class)
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.ANY)
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_CLASS)
public class StudasServiceIntegrationTest {

    @Autowired
    StudasService sService;

    @Autowired
    UserService uService;

    UserRequest request;

    @BeforeEach
    void setup() {
        request = new UserRequest("Test@1.x", "Test", "Peter");
    }

    @DirtiesContext(methodMode = DirtiesContext.MethodMode.BEFORE_METHOD)
    @Test
    void createNewStudas_asNewUser_returnsAsExpected() {
        UserResponse response = sService.createNewStudas(request);

        // Check id
        assertEquals(1, response.getId());
        assertEquals(request.getEmail(), response.getEmail());
        assertEquals(request.getLastname(), response.getLastname());
        assertEquals(request.getFirstname(), response.getFirstname());
    }

    @DirtiesContext(methodMode = DirtiesContext.MethodMode.BEFORE_METHOD)
    @Test
    void createStudas_ofExistingUser_behavesAsExpected() {
        // Add user
        uService.createNewUser(request);

        // Add user to students
        UserResponse response = sService.createNewStudas(request);

        // Check values
        assertEquals(1, response.getId());
        assertEquals(request.getEmail(), response.getEmail());
        assertEquals(request.getLastname(), response.getLastname());
        assertEquals(request.getFirstname(), response.getFirstname());
    }

    @DirtiesContext(methodMode = DirtiesContext.MethodMode.BEFORE_METHOD)
    @Test
    void createStudas_ofExistingStudas_returnsNull() {
        // Add student
        sService.createNewStudas(request);

        // Add same student again
        UserResponse response = sService.createNewStudas(request);
        assertNull(response); // Should be null
    }



}
