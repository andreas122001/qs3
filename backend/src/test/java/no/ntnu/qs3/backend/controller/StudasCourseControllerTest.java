package no.ntnu.qs3.backend.controller;

import no.ntnu.qs3.backend.BackendApplication;
import no.ntnu.qs3.backend.model.Course;
import no.ntnu.qs3.backend.model.CourseResponse;
import no.ntnu.qs3.backend.model.UserRequest;
import no.ntnu.qs3.backend.model.UserResponse;
import no.ntnu.qs3.backend.service.StudasCourseService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;

import java.util.ArrayList;
import java.util.List;

import static no.ntnu.qs3.backend.util.ModelConverter.courseToResponse;
import static no.ntnu.qs3.backend.util.ModelConverter.userRequestToResponse;
import static no.ntnu.qs3.backend.util.TestUtil.asJsonString;
import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.when;
import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.MOCK;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ActiveProfiles("test")
@SpringBootTest(webEnvironment = MOCK, classes = BackendApplication.class)
@AutoConfigureMockMvc(addFilters = false)
public class StudasCourseControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private StudasCourseService service;

    UserRequest request;
    UserResponse response;
    Course course;

    List<UserResponse> studases;
    List<CourseResponse> courses;

    @BeforeEach
    void setup() {
        request = new UserRequest("Test", "test", "test");
        response = userRequestToResponse(1, request);
        course = new Course("Test", "test", "test" ,1972);

        studases = new ArrayList<>();
        courses = new ArrayList<>();

        studases.add(response);
        courses.add(courseToResponse(course));
    }

    @Test
    void gettingStudasReturnsExpected() throws Exception {
        when(service.getStudasesAtCourse(1))
                .thenReturn(studases);

        mockMvc.perform(get("/courses/{id}/studas",1))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[0].email", is(request.getEmail())));
    }

    @Test
    void gettingCoursesReturnsExpected() throws Exception {
        when(service.getCoursesOfStudas(1))
                .thenReturn(courses);

        mockMvc.perform(get("/studas/1/courses"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[0].code", is(course.getCode())));
    }

    @Test
    void addStudasReturnsExpected() throws Exception {
        when(service.addStudas(1, request))
                .thenReturn(response);

        mockMvc.perform(post("/courses/1/studas")
                        .content(asJsonString(request))
                        .contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.email", is(request.getEmail())))
                .andExpect(jsonPath("$.id", is(1)));
    }

    @Test
    void deleteExistentStudasReturnsExpected() throws Exception {
        when(service.removeStudas(1, request))
                .thenReturn(response);

        mockMvc.perform(delete("/courses/1/studas")
                        .content(asJsonString(request))
                        .contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.email", is(request.getEmail())));
    }

    @Test
    void deleteNonExistentReturns404() throws Exception {
        mockMvc.perform(delete("/courses/1/studas")
                        .content(asJsonString(request))
                        .contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isNotFound())
                .andExpect(jsonPath("$").doesNotExist());
    }





}
