package no.ntnu.qs3.backend.service;


import no.ntnu.qs3.backend.BackendApplication;
import no.ntnu.qs3.backend.model.*;
import org.junit.jupiter.api.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ActiveProfiles("test")
@SpringBootTest(classes = BackendApplication.class)
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_CLASS)
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.ANY)
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class ExerciseServiceIntegrationTest {

    @Autowired
    ExerciseService exerciseService;

    @Autowired
    CourseService courseService;

    @Autowired
    StudentService studentService;

    static ExerciseRequest exerciseRequest = new ExerciseRequest("qs3");
    static CourseRequest courseRequest = new CourseRequest("Fullstack", "IDATT2105","desc", 2050);
    static UserRequest userRequest = new UserRequest("erik@email.net", "Eriksen", "Erik");

    static ExerciseResponse exerciseExampleResponse;
    static CourseResponse courseResponse;
    static UserResponse userResponse;

    @Order(1)
    @Test
    void setupData() {
        userResponse = studentService.createNewStudent(userRequest);
        courseResponse = courseService.createNewCourse(courseRequest);
        exerciseExampleResponse = new ExerciseResponse(1, courseResponse.getId(), exerciseRequest.getName());
    }

    @Order(2)
    @Test
    void creatingExercise_returnsExpected() {
        ExerciseResponse response = exerciseService.createNewExerciseAtCourse(courseResponse.getId(), exerciseRequest);

        assertEquals(1, response.getId());

        assertEquals(exerciseRequest.getName(), response.getName());
        assertEquals(courseResponse.getId(), response.getCourseId());
    }

    @Order(3)
    @Test
    void addingExerciseToStudentCompleted_returnsExpected() {
        ExerciseResponse response = exerciseService.addCompletedExerciseToStudent(userResponse.getId(), 1);

        assertEquals(exerciseExampleResponse, response);
    }

    @Order(4)
    @Test
    void gettingStudentCompleted_returnsExpected() {
        List<ExerciseResponse> response = exerciseService.getStudentCompletedExercises(userResponse.getId(), courseResponse.getId());

        assertEquals(1, response.size());

        assertEquals(exerciseExampleResponse, response.get(0));
    }
}
