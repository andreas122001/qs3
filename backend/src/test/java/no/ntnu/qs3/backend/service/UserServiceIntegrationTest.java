package no.ntnu.qs3.backend.service;

import no.ntnu.qs3.backend.BackendApplication;
import no.ntnu.qs3.backend.model.UserRequest;
import no.ntnu.qs3.backend.model.UserResponse;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.transaction.annotation.Transactional;

import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.MOCK;

@ActiveProfiles("test")
@SpringBootTest(classes = BackendApplication.class)
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.ANY)
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_CLASS)
public class UserServiceIntegrationTest {

    @Autowired
    UserService service;

    UserRequest request;

    @BeforeEach
    void setup() {
        request = new UserRequest("Test@Test.no", "Egil", "Jan");
    }

    @DirtiesContext(methodMode = DirtiesContext.MethodMode.BEFORE_METHOD)
    @Test
    @Transactional
    void createNonExistentUser_createsAUser_withExpectedValues() {
            UserResponse response = service.createNewUser(request);

            // First id is 1
            assertEquals(1, response.getId());

            // Response has same attributes
            assertEquals(request.getEmail(), response.getEmail());
            assertEquals(request.getFirstname(), response.getFirstname());
            assertEquals(request.getLastname(), response.getLastname());
    }

    @DirtiesContext(methodMode = DirtiesContext.MethodMode.BEFORE_METHOD)
    @Test
    @Transactional
    void givenAUser_createSaidUser_fails() {
        service.createNewUser(request);
        UserResponse response = service.createNewUser(request);

        assertNull(response);
    }

    @DirtiesContext(methodMode = DirtiesContext.MethodMode.BEFORE_METHOD)
    @Test
    @Transactional
    void addingTwoUsers_userIdIsCorrect() {
        UserResponse user1 = service.createNewUser(request);
        request.setEmail("test2@test.tk");
        UserResponse user2 = service.createNewUser(request);

        assertNotNull(user1);
        assertNotNull(user2);

        assertNotEquals(user1, user2);

        assertEquals(1, user1.getId());
        assertEquals(2, user2.getId());
    }

}
