package no.ntnu.qs3.backend.controller;

import no.ntnu.qs3.backend.BackendApplication;
import no.ntnu.qs3.backend.model.CourseRequest;
import no.ntnu.qs3.backend.model.CourseResponse;
import no.ntnu.qs3.backend.service.CourseService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;

import static no.ntnu.qs3.backend.util.TestUtil.asJsonString;
import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.when;
import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.MOCK;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ActiveProfiles("test")
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_CLASS)
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.ANY)
@SpringBootTest(webEnvironment = MOCK, classes = BackendApplication.class)
@AutoConfigureMockMvc(addFilters = false)
public class CourseControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private CourseService service;

    private CourseRequest request;
    private CourseResponse response;

    @BeforeEach
    void setup() {
        request = new CourseRequest("Test","Test","Test",1970);
        response = new CourseResponse(1,"Test","Test","Test",1970);
    }

    @Test
    void addingCourseWorks() throws Exception {
        when(service.createNewCourse(request))
                .thenReturn(response);

        mockMvc.perform(post("/courses")
                        .content(asJsonString(request))
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.name", is("Test")));
    }

    @Test
    void getNonExistentCourseReturns404() throws Exception {

        mockMvc.perform(get("/courses/1"))
                .andExpect(status().isNotFound());

        mockMvc.perform(get("/courses"))
                .andExpect(status().isNotFound());
    }

    @Test
    void getExistentCourseWorks() throws Exception {
        when(service.getById(1))
                .thenReturn(response);

        mockMvc.perform(get("/courses/1"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.code", is(request.getCode())));
    }

    @Test
    void deletingCourseWorks() throws Exception {
        when(service.deleteById(1))
                .thenReturn(response);

        mockMvc.perform(delete("/courses/{id}", 1))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.code", is(request.getCode())));
    }
}
