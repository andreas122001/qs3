package no.ntnu.qs3.backend.service;

import no.ntnu.qs3.backend.BackendApplication;
import no.ntnu.qs3.backend.model.CourseRequest;
import no.ntnu.qs3.backend.model.CourseResponse;
import no.ntnu.qs3.backend.model.UserRequest;
import no.ntnu.qs3.backend.model.UserResponse;
import org.junit.jupiter.api.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ActiveProfiles("test")
@SpringBootTest(classes = BackendApplication.class)
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_CLASS)
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.ANY)
public class StudentCourseServiceIntegrationTest {

    @Autowired
    StudentService sService;

    @Autowired
    CourseService cService;

    @Autowired
    StudentCourseService service;

    private UserRequest sRequest;
    private CourseRequest cRequest;
    private static UserResponse sResponse;
    private static CourseResponse cResponse;

    @BeforeEach
    void setup() {
        sRequest = new UserRequest("jb@s.no", "Berg", "Jale");
        cRequest = new CourseRequest("Skigåing", "SKI2050", "Gå på ski", 2020);
    }

    @Order(1)
    @Test
    void setupStudentAndCourse_works() {
        // Add student and course, get response
        sResponse = sService.createNewStudent(sRequest);
        cResponse = cService.createNewCourse(cRequest);
    }

    @Order(2)
    @Test
    void addingStudentToCourse_returnExpected() {
        UserResponse response = service.addStudent(cResponse.getId(), sRequest);

        assertEquals(sResponse, response);
    }

    @Order(3)
    @Test
    void gettingCoursesOfStudent_returnsExpected() {
        List<CourseResponse> courses = service.getCoursesOfStudent(sResponse.getId());

        assertEquals(1, courses.size());
        assertEquals(cResponse, courses.get(0));
    }

    @Order(4)
    @Test
    void gettingStudentsAtCourse_returnsExpected() {
        List<UserResponse> users = service.getStudentsAtCourse(cResponse.getId());

        assertEquals(1, users.size());
        assertEquals(sResponse, users.get(0));
    }
}
