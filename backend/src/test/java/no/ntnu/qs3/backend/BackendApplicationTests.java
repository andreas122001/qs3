package no.ntnu.qs3.backend;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.ComponentScans;

@SpringBootTest(classes = BackendApplication.class)
class BackendApplicationTests {

    @Test
    void contextLoads() {
    }

}
