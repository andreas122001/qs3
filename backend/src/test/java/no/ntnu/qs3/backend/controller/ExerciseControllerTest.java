package no.ntnu.qs3.backend.controller;

import no.ntnu.qs3.backend.BackendApplication;
import no.ntnu.qs3.backend.model.ExerciseRequest;
import no.ntnu.qs3.backend.model.ExerciseResponse;
import no.ntnu.qs3.backend.service.ExerciseService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;

import static no.ntnu.qs3.backend.util.TestUtil.asJsonString;
import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ActiveProfiles("test")
@SpringBootTest(classes = BackendApplication.class)
@AutoConfigureMockMvc(addFilters = false)
public class ExerciseControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    ExerciseService service;

    private ExerciseRequest request = new ExerciseRequest("Øving 1");
    private ExerciseResponse response = new ExerciseResponse(1, 1, "Øving 1");

    @Test
    void getOrDelete_nonExistent_returns404() throws Exception {
        mockMvc.perform(get("/courses/{id}/exercises/{id2}",1,1))
                .andExpect(status().isNotFound())
                .andExpect(jsonPath("$").doesNotExist());

        mockMvc.perform(delete("/courses/{id}/exercises/{id2}",1,1))
                .andExpect(status().isNotFound())
                .andExpect(jsonPath("$").doesNotExist());
    }

    @Test
    void getOrDelete_existent_returnsExpected() throws Exception {
        when(service.getExerciseAtCourseById(1,1))
                .thenReturn(response);

        when(service.deleteExerciseAtCourseById(1,1))
                .thenReturn(response);

        mockMvc.perform(get("/courses/{id}/exercises/{id2}",1,1))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id", is(response.getId())))
                .andExpect(jsonPath("$.courseId", is(response.getCourseId())))
                .andExpect(jsonPath("$.name", is(response.getName())));

        mockMvc.perform(delete("/courses/{id}/exercises/{id2}",1,1))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id", is(response.getId())))
                .andExpect(jsonPath("$.courseId", is(response.getCourseId())))
                .andExpect(jsonPath("$.name", is(response.getName())));
    }

    @Test
    void creatingExercise_whenAlreadyExists_returnsConflict() throws Exception {
        mockMvc.perform(post("/courses/{id}/exercises/",1)
                        .content(asJsonString(request))
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isConflict())
                .andExpect(jsonPath("$.id").doesNotExist());
    }
}
