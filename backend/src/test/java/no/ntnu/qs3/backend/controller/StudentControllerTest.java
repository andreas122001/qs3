package no.ntnu.qs3.backend.controller;


import no.ntnu.qs3.backend.BackendApplication;
import no.ntnu.qs3.backend.model.UserRequest;
import no.ntnu.qs3.backend.model.UserResponse;
import no.ntnu.qs3.backend.service.StudentService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;

import static no.ntnu.qs3.backend.util.ModelConverter.userRequestToResponse;
import static no.ntnu.qs3.backend.util.TestUtil.asJsonString;
import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.when;
import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.MOCK;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ActiveProfiles("test")
@SpringBootTest(webEnvironment = MOCK, classes = BackendApplication.class)
@AutoConfigureMockMvc(addFilters = false)
public class StudentControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private StudentService service;

    private UserRequest request = new UserRequest("test@test.no", "test", "test");
    private UserResponse response = userRequestToResponse(1, request);

    @Test
    void gettingNonExistentStudentReturns404() throws Exception {
        mockMvc.perform(get("/students/{id}", 1)
                    .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isNotFound())
                .andExpect(jsonPath("$").doesNotExist());
    }

    @Test
    void gettingExistentStudentReturnsAsExpected() throws Exception {

        when(service.getById(1))
                .thenReturn(response);

        mockMvc.perform(get("/students/{id}", 1)
                        .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.email", is("test@test.no")))
                .andExpect(jsonPath("$.firstname", is("test")))
                .andExpect(jsonPath("$.lastname", is("test")));
    }

    @Test
    void removingNonExistentReturns404() throws Exception {
        mockMvc.perform(post("/students/{id}/remove", 1))
                .andExpect(status().isNotFound());
    }

    @Test
    void removingStudentReturnsExpected() throws Exception {
        when(service.removeById(1))
                .thenReturn(response);

        mockMvc.perform(delete("/students/{id}", 1))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.email", is(request.getEmail())));
    }

    @Test
    void creatingNewStudentWorks() throws Exception {
        when(service.createNewStudent(request))
                .thenReturn(response);

        mockMvc.perform(post("/students")
                        .content(asJsonString(request))
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.email", is(request.getEmail())));
    }
}
