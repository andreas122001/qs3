# QS3
## Nyttige lenker:
- Lenke til nettside: https://andreas122001.gitlab.io/qs3
- Lenke til GitLab: https://gitlab.com/andreas122001/qs3
- Lenke til CI/CD-pipelines: https://gitlab.com/andreas122001/qs3/-/pipelines
- Lenke til Swagger doc: https://queues3api.tk:8443/api/swagger-ui/

Merk: for nettsida må URLen være helt lik den over. E.g. /qs3/home fungerer ikke!.


## Brukerinstrukser
For å teste applikasjonen er det opprettet to brukere som man kan bruke. Den ene er en student/studas, den andre administrator. De har følgende innloggingsinfo:

Student/studas: 
- Brukernavn: 'user'
- Passord: 'user'

Administrator:;
- Brukernavn: 'admin'
- Passord: 'admin'

MERK: APIet bruker ssl med selvsignert sertifikat. Dermet kan det hende at nettleseren blokkerer forespørsler mellom nettside og API-tjener. For å unngå dette må en skru av blokkering, eventuelt kjøre APIet lokal og skru av localhost-blokkering.

For å bruke lokalt API må en endre API-adressa i fila 'frontend/src/utils/config.js'. Her endrer du "https://queues3api.tk:8443/api" til "https://localhost:8443/api".

I tillegg kan det være nødvendig å bytte ut "produktion" i fila frontend/vue.config.js til "development"



## Funksjonalitet
Per nå er det ikke full funksjonalitet for å bruke nettsiden, ettersom vi har lagt mer tid på kodekvalitet, og da spesielt i backend. Backend har den meste av funksjonaliteten som trengs, med unntak av kø-funksjonalitet og øvingsregler, selv om dette er planlagt i databasediagrammet. 

Tjeneren har funksjonalitet for autorisasjon, men pga. problemer ved testing er det per nå skrudd av (kommentert ut i koden), men det er der. Det er implementert autentikasjon med jwt-tokens.

### Generelt i prosjektet
- Gitlab CICD (semi-compleks med bruk av needs og caching for effektiv pipeline)
- Ekstern linux server som tjener APIet.
- Autentikasjon med JWT-tokens.


#### Student
- Kan se emner de er del av
- Kan se øvinger på sine emner og om de er godkjente

#### Student assistent
- Kan se emner de assisterer i
- Kan se studenter i emnene de assisterer i
- Kan godkjenne og fjerne godkjenning av studenters øvinger

#### Administrator
- Kan se alle brukere og emner
- Kan se brukere på et gitt emne
- Kan laste opp en liste med studenter eller assistenter til et emne vha. en .csv-fil
- Kan legge til et emne
- Kan slette et emne
- Kan legge til en øving på et emne


## Framtidig arbeid
- Implementer et kø system i backend og frontend,
- Implementer form for å få oppgaver rettet, inkludert sitteplass o.l.,
- Implenter arkiverte fag,


